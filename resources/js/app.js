
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VeeValidate from 'vee-validate';
import VueAWN from "vue-awesome-notifications"
import VueSweetalert2 from 'vue-sweetalert2';

import VueStripeCheckout from 'vue-stripe-checkout';
const options = {
    key: 'pk_test_45W1I8JDDlITF12JAJaeHbnD',
    // image: 'https://cdn.meme.am/images/100x100/15882140.jpg',
    locale: 'auto',
    currency: 'PHP',
    billingAddress: false,
    panelLabel: 'Checkout {{amount}}'
}

Vue.use(VueStripeCheckout, options);

Vue.use(VeeValidate)
Vue.use(VueAWN)
Vue.use(VueSweetalert2)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('search-bus', require('./components/Main/Book.vue'));
Vue.component('seat-bus-create', require('./components/Seat/Create.vue'));
Vue.component('seat-bus-edit', require('./components/Seat/Edit.vue'));

Vue.component('trip-create', require('./components/Trip/Create.vue'));
Vue.component('trip-edit', require('./components/Trip/Edit.vue'));
Vue.component('trip-search', require('./components/Trip/Search.vue'));
Vue.component('trip-book', require('./components/Trip/Book.vue'));

Vue.component('ticket-create', require('./components/Ticket/Create.vue'));

const app = new Vue({
    el: '#app',
    data(){
        return {
            liners: [],
            buses: [],
            provinces: [],
            paypal: {
                sandbox: 'AQcoa0xCrRVaUEWLwBypRVbZA1HHXNQ33rR9Zecxea7r52U0l-BIfcPTCavGhl_6jM56hVRX0i27INQH',
                production: '',
            }
        }
    },
    computed: {
        roles: function(){
            return window.Laravel.roles
        },
        user: function(){
            return window.Laravel.user
        }
    },
    methods: {
        role(key){
            return this.roles.find(x => x == key);
        },
        async getLiners(){
            const { data } = await axios.post('/admin/api/liners/get');

            if(data.status){
                this.liners = data.results;
            }
        },
        async getBuses(){
            const { data } = await axios.post('/admin/api/buses/get');

            if(data.status){
                this.buses = data.results
            }
        },
        async getProvinces(){
            const { data } = await axios.post('/api/locations/provinces');

            if(data.status){
                this.provinces = data.results;
            }
        },
        generate(seat_map, reservations = []){
            if(!seat_map) return console.error("Seat Plan: Vihicle map not provided");
            let seat_count = 0,
                parse_seats = [];
            for(var a = 0; a < seat_map.length; a++){
                let g = [],
                    seats = seat_map[a].split("");
                for(var d = 0; d < seats.length; d++){
                    let seat = {
                        reserved: false,
                        selected: false,
                        space: false,
                        status: false,
                    }
                    if("_" !== seats[d]){
                        if(seat_count++, seat.num = seat_count, seat.type = seats[d], reservations.length > 0){
                            for( var j = 0; j < reservations.length; j++){
                                reservations[j].reserved_seats.indexOf(seat_count) !== -1 && (seat.status = reservations[j].status, seat.reserved = true)
                            }
                        }
                    }else{
                        seat.space = true;
                    }
                    g.push(seat);
                }
                parse_seats.push(g);
            }
            return parse_seats;
        },
    }
});
