@extends('layouts.app')

@section('top_content')
    <div class="jumbotron mb-0">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <search-bus></search-bus>
                </div>
                <div class="col-md-6">
                    <img src="{{ asset('images/bus2.png') }}" class="img-fluid" alt="Bus Vector">
                </div>
            </div>
        </div>
    </div>
    <div class="lower-bar mb-4">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <i class="fa fa-bus fa-2x"></i>
                    <span>400 Routes</span>
                </div>
                <div class="col-md-4">
                    <i class="fa fa-users fa-2x"></i>
                    <span>200 Operators</span>
                </div>
                <div class="col-md-4">
                    <i class="fa fa-ticket-alt fa-2x"></i>
                    <span>10,000 Ticket sold</span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <p>
        Central Bicol Station Online Bus Ticketing and Reservation System’s purpose is to let the customers make reservation for the bus ticket via online, check bus availability and the administrator can update, delete and view. Customers can buy ticket and make payment online and make seat reservation via Stripe- Paypal so that there is no need for them to go counter for payment. This System is develop using diagrams such as Data Flow Diagram (DFD), Entity Relationship Diagram (ERD).
    </p>
    <p>
        Ubuntu as operating system will be used so that software tools should be usable by people in their local language and despite any disabilities, and that people will have the freedom to customize. Bootstrap will be using to design the system. Traveling is a large growing business across all countries. Bus reservation system deals with maintenance of records of details of each passenger. It also includes maintenance of information like schedule and details of each bus. We observed the working of the Bus reservation system and after going through it, we get to know that there are many operations, which they have to do manually. It takes a lot of time and causing many errors while data entry.
    </p>
    <p>
        Due to this, sometimes a lot of problems occur and they were facing many disputes with customers. To solve the above problem, and further maintaining records of passenger details, seat availability, price per seat, bill generation and other things, we are offering this proposal of system. By using this software, customer can easily reserve tickets via internet. Customer can check availability of bus and reserve selective seats. The project provides and checks all sorts of constraints so that user does give only useful data and thus validation is done in an effective way.
    </p>
@endsection
