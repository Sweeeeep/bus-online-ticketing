@extends('layouts.app')


@section('content')
    <trip-search :search-query='@json($search)'></trip-search>
@endsection
