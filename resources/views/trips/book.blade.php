@extends('layouts.app')

@section('content')
    <trip-book :reservation-info='@json($reservationInfo)'></trip-book>
@endsection
