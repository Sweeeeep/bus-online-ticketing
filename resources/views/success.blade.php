@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card">
                <div class="card-header">
                    Transaction Successful
                </div>
                <div class="card-body text-center success-body">
                    <i class="far fa-check-circle fa-5x"></i>
                    <div class="success-title"><b>Thank you for your purchase</b></div>
                    <p>
                        Your Ticket Number is <b>{{ $invoice->ticket->ticket_no }}</b> <br>
                        We email your invoice reciept with eTicket details
                    </p>
                    <a href="/" class="btn btn-primary">Return to Home</a>
                </div>
            </div>
        </div>
    </div>
@endsection
