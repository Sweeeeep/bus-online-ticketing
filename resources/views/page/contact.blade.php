@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-8 offset-md-2">
        <div class="card">
            <div class="card-header">
                Contact us
            </div>
            <div class="card-body">
                <form action="">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Mobile Number</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Reasons</label>
                        <select name="" class="form-control">
                            <option value="1">Rebooking</option>
                            <option value="2">Cancellation</option>
                            <option value="3">Others</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Other question or message</label>
                        <textarea name="" class="form-control" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
