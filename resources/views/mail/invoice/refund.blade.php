@component('mail::message')
# Hello {{ $invoice->customer->full_name }},

One of your invoices has been refunded

## Invoice Reciept
Invoice #{{ $invoice->invoice_no }}

@component('mail::table')
| Amount Refunded  |Date Refunded | Payment Method |
|:--------     |:-------- |:--------       |
| {{ number_format($invoice->amount, 2) }} PHP | {{ $invoice->updated_at->format('M d, Y') }} | {{ ($invoice->type == 1 ? $invoice->card_brand . ' - ' . $invoice->card_last_four : 'Paypal - '. $invoice->paypal_email) }} |
@endcomponent


Thanks,<br>
{{ config('app.name') }}
@endcomponent
