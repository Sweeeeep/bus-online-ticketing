@component('mail::message')
# Hello {{ $invoice->customer->full_name }},
Thank you for your using our service, Below is your reciept and link to your eTicket

## Invoice Reciept
Invoice #{{ $invoice->invoice_no }}

@component('mail::table')
| Amount Paid  |Date Paid | Payment Method |
|:--------     |:-------- |:--------       |
| {{ number_format($invoice->amount, 2) }} PHP | {{ $invoice->created_at->format('M d, Y') }} | {{ ($invoice->type == 1 ? $invoice->card_brand . ' - ' . $invoice->card_last_four : 'Paypal - '. $invoice->paypal_email) }} |
@endcomponent

## eTicket Infomation below

Please print out this eTicket

@component('mail::button', ['url' => route('ticket.information', [
    'ticket' => $invoice->ticket_id,
    'checkout_id' => $invoice->transaction_id
])])
eTicket
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
