@extends('admin.layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">List of {{ request('type') == 'archives' ? 'Archive' : 'Active' }} Trips</h3>
        </div>
        @include('flash::message')
        <table class="table table-striped mb-0">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Route</th>
                    <th scope="col">Departure Time</th>
                    <th scope="col">Available Seats</th>
                    @if(request('type') != 'archives')
                        <th scope="col">Action</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                @foreach($trips as $trip)
                    <tr>
                        <th scope="row">{{ $trip->id }}</th>
                        <td>
                            @foreach($trip->routes->take(2) as $key => $route)
                                @php
                                    if($route->terminal){
                                        $city = $route->terminal->city;
                                    }else{
                                        $city = '';
                                    }
                                @endphp
                                {{ optional($city)->name ?? '-' }} {!! $key == 0 ? '<i class="fe fe-arrow-right"></i>' : '' !!}
                            @endforeach
                        </td>
                        <td>{{ $trip->depart_at->format('d/m/Y @ g:i A') }}</td>
                        <td>{{ $trip->available_seats }}</td>
                        @if(request('type') != 'archives')
                        <td>
                            <form action="{{ route('admin.trips.destroy', $trip->id) }}" method="POST">
                                <a href="{{ route('admin.trips.edit', $trip->id) }}" class="btn btn-secondary btn-sm">Update</a>
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete?')">Delete</button>
                            </form>
                        </td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    {{ $trips->render() }}
@endsection
