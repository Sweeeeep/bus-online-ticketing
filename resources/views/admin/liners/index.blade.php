@extends('admin.layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">List of Liners</h3>
        </div>
        @include('flash::message')
        <table class="table table-striped mb-0">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Total Bus</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($liners as $liner)
                    <tr>
                        <th scope="row">{{ $liner->id }}</th>
                        <td>{{ $liner->name }}</td>
                        <td>{{ $liner->buses_count }}</td>
                        <td>
                            <form action="{{ route('admin.liners.destroy', $liner->id) }}" method="POST">
                                <a href="{{ route('admin.liners.edit', $liner->id) }}" class="btn btn-secondary btn-sm">Update</a>
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    {{ $liners->render() }}
@endsection
