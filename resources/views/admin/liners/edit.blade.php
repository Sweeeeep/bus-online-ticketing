@extends('admin.layouts.admin')

@section('content')
    <form class="card" method="POST" action="{{ route('admin.liners.update', $liner->id) }}" enctype="multipart/form-data">
        <div class="card-header">
            <h3 class="card-title">Edit '<b>{{ $liner->name }}</b>' Liner</h3>
        </div>
        @include('flash::message')
        <div class="card-body">
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Name</label>
                    <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $liner->name }}">
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group col-md-5">
                    <label>Mobile Number</label>
                    <input type="text" class="form-control{{ $errors->has('mobile_number') ? ' is-invalid' : '' }}" name="mobile_number" value="{{ $liner->mobile_number }}">
                    @if ($errors->has('mobile_number'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('mobile_number') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group col-md-5">
                    <label>Address</label>
                    <textarea class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" rows="2">{{ $liner->address }} </textarea>
                    @if ($errors->has('address'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('address') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group col-md-3">
                    <label>Logo</label>
                    <input type="file" class="form-control-file{{ $errors->has('logo') ? ' is-invalid' : '' }}" name="logo">
                    @if ($errors->has('logo'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('logo') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group col-md-3">
                    Current Logo: <br>
                    <img src="{{ $liner->logo_url }}" width="100px" alt=""><br>
                </div>
            </div>
        </div>
        <div class="card-footer">
            @csrf
            <button class="btn btn-primary" type="submit">Submit</button>
        </div>
    </form>
@endsection
