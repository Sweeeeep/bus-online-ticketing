@extends('admin.layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Ticket # {{ $ticket->ticket_no }}</h3>
            <div class="card-options">
                <a href="{{ route('admin.invoice.view', $ticket->invoice->id) }}" class="btn btn-primary btn-sm">View Invoice</a>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 offset-md-3">
                    <table class="table">
                        <thead>
                            <tr>
                                <th colspan="2">Ticket Information</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td scope="row" width="40%">Reserved Seats</td>
                                <td>
                                    {{ implode($ticket->reserved_seats, ', ') }}
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">Boarding</td>
                                @php
                                    $boarding = $ticket->trip->routes->where('type', 0)->first();
                                @endphp
                                <td>
                                    {{ optional($boarding->terminal)->name }}
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">Dropping</td>
                                <td>
                                    {{ optional($ticket->route->terminal)->name }}
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">Trip Date</td>
                                <td>
                                    {{ $ticket->trip->depart_at->format('M d, Y @ h:m A') }}
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">Status</td>
                                <td>
                                    {{ $ticket->ticket_status }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table">
                        <thead>
                            <tr>
                                <th colspan="2">Customer Information</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $customer = $ticket->customer;
                            @endphp
                            <tr>
                                <td scope="row">Full Name</td>
                                <td>
                                    {{ $customer->full_name }}
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">Email</td>
                                <td>
                                    {{ $customer->email }}
                                </td>
                            </tr>
                            <tr>
                                <td scope="row">Mobile Number</td>
                                <td>
                                    {{ $customer->mobile_number }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table">
                        <thead>
                            <tr>
                                <th colspan="2">Passengers</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ticket->passengers as $passenger)
                            <tr>
                                <td scope="row">Passenger #{{$loop->iteration}}</td>
                                <td>
                                    {{ $passenger->name }} (Seat #{{ $passenger->seat_no }})
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
