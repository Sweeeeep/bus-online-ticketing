@extends('admin.layouts.admin')

@section('content')
<div class="card">
    <div class="card-body">
        <form>
            <div class="form-group mb-0">
                <div class="input-group">
                    <input type="text" class="form-control" name="q" placeholder="Search for ticket id">
                    <span class="input-group-append">
                        <button class="btn btn-primary" type="submit">Go!</button>
                    </span>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">
            Tickets
        </h3>
    </div>
    <div class="table-responsive">
        <table class="table mb-0">
            <thead>
                <tr>
                    <th>Ticket ID</th>
                    <th>Boarding</th>
                    <th>Dropping</th>
                    <th>Reserved Seats</th>
                    <th class="text-center">Amount</th>
                    <th class="text-center"><i class="fe fe-settings"></i></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($tickets as $ticket)
                    <tr>
                        <td scope="row">
                            {{ $ticket->ticket_no }}
                        </td>
                        @php
                            $boarding = $ticket->trip->routes->where('type', 0)->first();
                        @endphp
                        <td>
                            {{ optional($boarding->terminal)->name ?? '-' }}
                        </td>
                        <td>
                            {{ optional($ticket->route)->terminal->name ?? '-' }}
                        </td>
                        <td>
                            {{ implode($ticket->reserved_seats, ', ') }}
                        </td>
                        <td class="text-center">
                            {{ number_format($ticket->amount, 2) }}
                        </td>
                        <td class="text-center">
                            <a href="{{ route('admin.ticket.view', $ticket->id) }}" class="btn btn-secondary btn-sm">
                                View
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
{{ $tickets->render()}}
@endsection
