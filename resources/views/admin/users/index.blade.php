@extends('admin.layouts.admin')


@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">List of Users</h3>
    </div>
    @include('flash::message')
    <table class="table table-striped mb-0">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Roles</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
                <tr>
                    <th scope="row">{{ $user->id }}</th>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                        @foreach ($user->roles as $role)
                            {{ $role->name == 'tenant' ? $role->name . ' - ' . optional($user->liner)->name : $role->name  }}
                        @endforeach
                    </td>
                    {{-- <td>{{ $user->liner_id ? $user->liner->name : 'Admin' }}</td> --}}
                    <td>
                        <form action="{{ route('admin.users.destroy', $user->id) }}" method="POST">
                        <a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-secondary btn-sm">Update</a>
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete?')">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
{{ $users->render() }}
@endsection
