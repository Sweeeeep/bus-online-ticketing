@extends('admin.layouts.admin')

@section('content')
    <form class="card" method="POST" action="{{ route('admin.users.update', $user->id) }}" enctype="multipart/form-data">
        <div class="card-header">
            <h3 class="card-title">Update User {{ $user->name }}</h3>
        </div>
        @include('flash::message')
        <div class="card-body">
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Name</label>
                    <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $user->name }}">
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label>Email</label>
                    <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $user->email }}">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label>Password</label>
                    <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Leave blank if you don't want to change it">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label>Liner</label>
                    <select name="liner" class="form-control{{ $errors->has('liner') ? ' is-invalid' : '' }}">
                        <option value=""></option>
                        @foreach ($liners as $liner)
                            <option value="{{ $liner->id }}" {{ $liner->id == $user->liner_id ? 'selected' : '' }}>{{ $liner->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('liner'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('liner') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group col-md-6">
                    <label>Roles</label>
                    <select name="roles" class="form-control{{ $errors->has('roles') ? ' is-invalid' : '' }}">
                        @foreach ($roles as $role)
                            <option value="{{ $role->name }}" {{ $user->roles->where('id', $role->id)->first() ? 'selected' : '' }}>{{ $role->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('roles'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('roles') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
        <div class="card-footer">
            @csrf
            <button class="btn btn-primary" type="submit">Update</button>
        </div>
    </form>
@endsection
