@extends('admin.layouts.admin')


@section('content')
<form class="card" method="POST" action="{{ route('admin.buses.store') }}">
    <div class="card-header">
        <h3 class="card-title">Add Bus Details</h3>
    </div>
    @include('flash::message')
    <div class="card-body">
        <div class="row">
            <div class="form-group col-md-6">
                <label class="form-label">Bus Name</label>
                <input type="text" name="bus_name" class="form-control{{ $errors->has('bus_name') ? ' is-invalid' : '' }}" placeholder="Bus Name" value="{{ old('bus_name') }}">
                @if ($errors->has('bus_name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('bus_name') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-md-6">
                <label class="form-label">Bus Number</label>
                <input type="text" name="bus_number" class="form-control{{ $errors->has('bus_number') ? ' is-invalid' : '' }}" placeholder="Bus Number" value="{{ old('bus_number') }}">
                @if ($errors->has('bus_number'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('bus_number') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-md-6">
                <label class="form-label">Seat Layout</label>
                <select class="form-control{{ $errors->has('seat_layout') ? ' is-invalid' : '' }}" name="seat_layout">
                    <option value="" disabled selected>--Select Seat Layout--</option>
                    @foreach($layouts as $layout)
                        <option value="{{ $layout->id }}" {{ old('seat_layout') ? (old('seat_layout') == $layout->id ? 'selected' : '') : '' }}>{{ $layout->title }} {{ auth()->user()->hasRole('admin') ? '- '.$layout->liner->name : '' }}</option>
                    @endforeach
                </select>
                @if ($errors->has('seat_layout'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('seat_layout') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-md-6">
                <label class="form-label">Bus Type</label>
                <select class="form-control{{ $errors->has('bus_type') ? ' is-invalid' : '' }}" name="bus_type">
                    <option value="" disabled selected>--Select Bus Type--</option>
                    @foreach(BusHelpers::BusType as $key => $type)
                    <option value="{{ $key }}" {{ old('bus_type') ? (old('bus_type') == $key ? 'selected' : '') : '' }}>{{ $type }}</option>
                    @endforeach
                </select>
                @if ($errors->has('bus_type'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('bus_type') }}</strong>
                </span>
                @endif
            </div>
            @if(auth()->user()->hasRole('admin'))
                <div class="form-group col-md-6">
                    <label class="form-label">Bus Liners</label>
                    <select class="form-control{{ $errors->has('bus_liner') ? ' is-invalid' : '' }}" name="bus_liner">
                        <option value="" disabled selected>--Select Bus Liner--</option>
                        @foreach($liners as $liner)
                            <option value="{{ $liner->id }}" {{ old('bus_liner') ? (old('bus_liner') == $liner->id ? 'selected' : '') : '' }}>{{ $liner->name }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('bus_liner'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('bus_liner') }}</strong>
                        </span>
                    @endif
                </div>
            @endif
            <div class="form-group col-md-6">
                <div class="form-group">
                    <div class="form-label">Bus Amenities</div>
                    <div>
                    @foreach (BusHelpers::Amenities as $key => $amenitie)
                        <label class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" name="bus_amenities[]" value="{{ $key }}" {{ old('bus_amenities') ? (in_array($key, old('bus_amenities')) ? 'checked' : '') : ''  }}>
                            <span class="custom-control-label">{{ $amenitie }}</span>
                        </label>
                    @endforeach
                    </div>
                </div>
                @if ($errors->has('bus_amenities'))
                    <span class="invalid-feedback d-block" role="alert">
                        <strong>{{ $errors->first('bus_amenities') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    <div class="card-footer">
        @csrf
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>
@endsection
