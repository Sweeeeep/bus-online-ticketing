@extends('admin.layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">List of Buses</h3>
        </div>
        @include('flash::message')
        <table class="table table-striped mb-0">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Bus Name</th>
                    <th scope="col">Bus Number</th>
                    <th scope="col">Bus Liner</th>
                    <th scope="col">Seat Capacity</th>
                    <th scope="col">Seat Layout Name</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($buses as $bus)
                    <tr>
                        <th scope="row">{{ $bus->id }}</th>
                        <td>{{ $bus->name }}</td>
                        <td>{{ $bus->body_number }}</td>
                        <td>{{ optional($bus->liner)->name }}</td>
                        <td>{{ $bus->seat_capacity }}</td>
                        <td>{{ optional($bus->seatlayout)->title }}</td>
                        <td>
                            <form action="{{ route('admin.buses.destroy', $bus->id) }}" method="POST">
                                <a href="{{ route('admin.buses.edit', $bus->id) }}" class="btn btn-secondary btn-sm">Update</a>
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    {{ $buses->render() }}
@endsection
