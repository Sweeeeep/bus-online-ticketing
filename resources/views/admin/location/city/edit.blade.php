@extends('admin.layouts.admin')

@section('content')
    <form class="card" method="POST" action="{{ route('admin.city.update', [
        'cityID' => $city->id,
        'provinceID' => $city->province_id
    ]) }}" enctype="multipart/form-data">
        <div class="card-header">
            <h3 class="card-title">Edit City {{ $city->name }}</h3>
        </div>
        @include('flash::message')
        <div class="card-body">
            <div class="row">
                <div class="form-group col-md-12">
                    <label>Name</label>
                    <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $city->name }}">
                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
        <div class="card-footer">
            @csrf
            <button class="btn btn-primary" type="submit">Submit</button>
        </div>
    </form>
@endsection
