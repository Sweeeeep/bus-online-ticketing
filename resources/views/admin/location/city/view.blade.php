@extends('admin.layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">
            City of {{ $city->name }} Information
        </h3>
    </div>
    @include('flash::message')
    <div class="card-body">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <a href="{{ route('admin.terminal.create', $city->id) }}" class="btn btn-primary btn-sm float-right">Add new Terminal</a>
                <h4>List of Terminals</h4>
                <table class="table mb-0">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($city->terminals as $terminal)
                        <tr>
                            <th scope="row">{{ $terminal->id }}</th>
                            <td>{{ $terminal->name }}</td>
                            <td>
                                <form action="{{ route('admin.terminal.destroy', [
                                    'cityID' => $city->id,
                                    'terminalID' => $terminal->id
                                ]) }}" method="POST">
                                    <a href="{{ route('admin.terminal.edit', [
                                        'cityID' => $city->id,
                                        'terminalID' => $terminal->id
                                    ]) }}" class="btn btn-secondary btn-sm">Update</a>
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete?')">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
