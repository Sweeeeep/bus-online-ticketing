@extends('admin.layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">
            Province of {{ $province->name }} Information
        </h3>
    </div>
    @include('flash::message')
    <div class="card-body">
        <div class="row">
            <div class="col-md-8 offset-md-2">
            <a href="{{ route('admin.city.create', $province->id) }}" class="btn btn-primary btn-sm float-right">Add new City</a>
                <h4>List of Cities</h4>
                <table class="table mb-0">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Total Terminal</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($province->cities as $city)
                        <tr>
                            <th scope="row">{{ $city->id }}</th>
                            <td>{{ $city->name }}</td>
                            <td>{{ $city->terminals_count }}</td>
                            <td>
                                <form action="{{ route('admin.city.destroy', [
                                        'provinceID' => $province->id,
                                        'cityID' => $city->id
                                    ]) }}" method="POST">
                                    <a href="{{ route('admin.city.view', $city->id) }}" class="btn btn-secondary btn-sm">View</a>
                                    <a href="{{ route('admin.city.edit', [
                                        'provinceID' => $province->id,
                                        'cityID' => $city->id
                                    ]) }}" class="btn btn-secondary btn-sm">Update</a>
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete?')">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
@endsection
