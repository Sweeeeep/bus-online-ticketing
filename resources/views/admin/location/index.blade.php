@extends('admin.layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">List of Province</h3>
            <div class="card-options">
                <a href="{{ route('admin.province.create') }}" class="btn btn-primary btn-sm">Add new Province</a>
            </div>
        </div>
        @include('flash::message')
        <table class="table table-striped mb-0">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Province</th>
                    <th scope="col">Total City</th>
                    <th scope="col">Total Terminals</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($provinces as $province)
                    <tr>
                        <th scope="row">{{ $province->id }}</th>
                        <td>{{ $province->name }}</td>
                        <td>{{ $province->cities_count }}</td>
                        <td>{{ $province->terminals_count }}</td>
                        <td>
                            <form action="{{ route('admin.province.destroy', $province->id) }}" method="POST">
                                <a href="{{ route('admin.province.view', $province->id) }}" class="btn btn-secondary btn-sm">View</a>
                                <a href="{{ route('admin.province.edit', $province->id) }}" class="btn btn-secondary btn-sm">Update</a>
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete?')">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    {{ $provinces->render() }}
@endsection
