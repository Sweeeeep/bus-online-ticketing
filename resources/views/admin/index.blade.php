@extends('admin.layouts.admin')


@section('content')
<div class="row">
    <div class="col-md-4">
        <div class="card p-3">
            <div class="d-flex align-items-center">
                <span class="stamp stamp-md bg-blue mr-3">
                    <i class="fe fe-credit-card"></i>
                </span>
                <div>
                    <h4 class="m-0">{{ $ticketPaid }} <small>Ticket Sales</small></h4>
                    <small class="text-muted">{{ $ticketPending }} ticket pending</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card p-3">
            <div class="d-flex align-items-center">
                <span class="stamp stamp-md bg-blue mr-3">
                    <i class="fe fe-dollar-sign"></i>
                </span>
                <div>
                    <h4 class="m-0">₱ {{ number_format($dailyEarnings->total_amount, 2) }} <small>Daily Earning</small></h4>
                    <small class="text-muted">{{ $ticketPending }} waiting payments</small>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card p-3">
            <div class="d-flex align-items-center">
                <span class="stamp stamp-md bg-blue mr-3">
                    <i class="fe fe-dollar-sign"></i>
                </span>
                <div>
                    <h4 class="m-0">{{ $activeTrips }} <small>Pending trips today</small></h4>
                    <small class="text-muted">{{ $closeTrip }} success trips today</small>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="table-responsive">
        <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
            <thead>
                <tr>
                    <th class="text-center w-1"><i class="icon-people"></i></th>
                    <th>Customer</th>
                    <th>Invoice</th>
                    <th>Amount</th>
                    <th class="text-center">Payment Method</th>
                    <th class="text-center"><i class="fe fe-settings"></i></th>
                </tr>
            </thead>
            <tbody>
                @foreach($invoices as $invoice)
                    <tr>
                        <td class="text-center">
                            <span class="avatar">{{ $invoice->customer->name_initials }}</span>
                        </td>
                        <td>
                            <div>{{ $invoice->customer->full_name }}</div>
                            <div class="small text-muted">
                                Created at: {{ $invoice->customer->created_at->format('M d, Y') }}
                            </div>
                        </td>
                        <td>
                            <div class="small text-muted">Invoice Number</div>
                            <div>{{ $invoice->invoice_no }}</div>
                        </td>
                        <td>
                            <div class="small text-muted">{{ count($invoice->ticket->reserved_seats) }} reserved ticket</div>
                            <div>PHP {{ number_format($invoice->amount, 2) }}</div>
                        </td>
                        <td class="text-center">
                            @if($invoice->type == 1)
                                <i class="payment payment-{{ strtolower($invoice->card_brand) }}"></i>
                            @elseif($invoice->type == 2)
                                <i class="payment payment-paypal"></i>
                            @else
                                Cash
                            @endif
                        </td>
                        <td class="text-center">
                            <a href="{{ route('admin.invoice.view', $invoice->id) }}" class="btn btn-secondary btn-sm">View</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
{{ $invoices->setPath(route('admin.invoice.index'))->render()}}

{{-- <div class="card">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">First</th>
                <th scope="col">Last</th>
                <th scope="col">Handle</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
            </tr>
            <tr>
                <th scope="row">3</th>
                <td>Larry</td>
                <td>the Bird</td>
                <td>@twitter</td>
            </tr>
        </tbody>
    </table>
</div> --}}
@endsection
