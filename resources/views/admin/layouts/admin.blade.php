<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }} - Admin Panel</title>
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="{{ mix('css/admin.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app" class="page">
        <div class="page-main">
            <div class="header py-4">
                <div class="container">
                    <div class="d-flex">
                    <a class="header-brand" href="{{ route('admin.index') }}">
                        {{ config('app.name') }} Admin Panel
                    </a>
                    <div class="d-flex order-lg-2 ml-auto">
                        <div class="nav-item d-none d-md-flex">
                            <a class="btn btn-sm btn-outline-primary" href="{{ url('/') }}">Visit Homepage</a>
                        </div>
                        <div class="nav-item d-none d-md-flex">
                            <a class="btn btn-sm btn-outline-danger" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                        </div>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                    <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
                        <span class="header-toggler-icon"></span>
                    </a>
                    </div>
                </div>
            </div>
            <div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg order-lg-first">
                            <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                                <li class="nav-item">
                                    <a href="{{ route('admin.index') }}" class="nav-link {{ BusHelpers::isActiveRoute('admin.index') }}"><i class="fe fe-home"></i> Home</a>
                                </li>
                                @if(auth()->user()->hasRole('admin'))
                                <li class="nav-item">
                                    <a href="Javascript:void(0)" class="nav-link {{ BusHelpers::areActiveRoutes([
                                        'admin.liners.index', 'admin.liners.create'
                                    ]) }}" data-toggle="dropdown"><i class="fe fe-bar-chart-2"></i> Liners</a>
                                    <div class="dropdown-menu dropdown-menu-arrow">
                                        <a href="{{ route('admin.liners.index') }}" class="dropdown-item ">View All</a>
                                        <a href="{{ route('admin.liners.create') }}" class="dropdown-item ">Add New</a>
                                    </div>
                                </li>
                                @endif
                                <li class="nav-item dropdown">
                                    <a href="Javascript:void(0)" class="nav-link {{ BusHelpers::areActiveRoutes([
                                        'admin.buses.index', 'admin.buses.create'
                                    ]) }}" data-toggle="dropdown"><i class="fe fe-truck"></i> Buses</a>
                                    <div class="dropdown-menu dropdown-menu-arrow">
                                        <a href="{{ route('admin.buses.index') }}" class="dropdown-item ">View All</a>
                                        <a href="{{ route('admin.buses.create') }}" class="dropdown-item ">Add New</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="Javascript:void(0)" class="nav-link {{ BusHelpers::areActiveRoutes([
                                        'admin.trips.index', 'admin.trips.create'
                                    ]) }}" data-toggle="dropdown"><i class="fe fe-maximize-2"></i> Trips</a>
                                    <div class="dropdown-menu dropdown-menu-arrow">
                                        <a href="{{ route('admin.trips.index') }}" class="dropdown-item ">View All Active</a>
                                        <a href="{{ route('admin.trips.create') }}" class="dropdown-item ">Add New</a>
                                        <a href="{{ route('admin.trips.index', ['type' => 'archives']) }}" class="dropdown-item ">Archives</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="Javascript:void(0)" class="nav-link {{ BusHelpers::areActiveRoutes([
                                        'admin.seats.index', 'admin.seats.create'
                                    ]) }}" data-toggle="dropdown"><i class="fe fe-layout"></i> Seat Layouts</a>
                                    <div class="dropdown-menu dropdown-menu-arrow">
                                        <a href="{{ route('admin.seats.index') }}" class="dropdown-item ">View All</a>
                                        <a href="{{ route('admin.seats.create') }}" class="dropdown-item ">Add New</a>
                                    </div>
                                </li>
                                @if(auth()->user()->hasRole('admin'))
                                <li class="nav-item dropdown">
                                    <a href="Javascript:void(0)" class="nav-link {{ BusHelpers::areActiveRoutes([
                                        'admin.locations.index', 'admin.province.create', 'admin.province.edit', 'admin.province.view', 'admin.city.create', 'admin.city.edit', 'admin.city.view', 'admin.terminal.create', 'admin.terminal.edit'
                                    ]) }}" data-toggle="dropdown"><i class="fe fe-globe"></i> Locations</a>
                                    <div class="dropdown-menu dropdown-menu-arrow">
                                        <a href="{{ route('admin.locations.index') }}" class="dropdown-item ">View All</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="Javascript:void(0)" class="nav-link {{ BusHelpers::areActiveRoutes([
                                        'admin.users.index', 'admin.users.create'
                                    ]) }}" data-toggle="dropdown"><i class="fe fe-user"></i> Users</a>
                                    <div class="dropdown-menu dropdown-menu-arrow">
                                        <a href="{{ route('admin.users.index') }}" class="dropdown-item ">View All</a>
                                        <a href="{{ route('admin.users.create') }}" class="dropdown-item ">Add new</a>
                                    </div>
                                </li>
                                @endif
                                <li class="nav-item dropdown">
                                    <a href="Javascript:void(0)" class="nav-link {{ BusHelpers::areActiveRoutes([
                                        'admin.invoice.index'
                                    ]) }}" data-toggle="dropdown"><i class="fe fe-file-text"></i> Invoices</a>
                                    <div class="dropdown-menu dropdown-menu-arrow">
                                        <a href="{{ route('admin.invoice.index') }}" class="dropdown-item ">View All</a>
                                    </div>
                                </li>
                                <li class="nav-item dropdown">
                                    <a href="Javascript:void(0)" class="nav-link {{ BusHelpers::areActiveRoutes([
                                        'admin.ticket.index', 'admin.ticket.create'
                                    ]) }}" data-toggle="dropdown"><i class="fe fe-align-left"></i> Tickets</a>
                                    <div class="dropdown-menu dropdown-menu-arrow">
                                        <a href="{{ route('admin.ticket.index') }}" class="dropdown-item ">View All</a>
                                        <a href="{{ route('admin.ticket.create') }}" class="dropdown-item ">Add new Ticket</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <main class="pt-5">
                <div class="container">
                    @yield('content')
                </div>
            </main>
            <footer class="container">
                <p>© {{ config('app.name') }} 2018</p>
            </footer>
        </div>
    </div>
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'user' => auth()->user()->only('id', 'liner_id'),
            'roles' => auth()->user()->getRoleNames()
        ]) !!}
    </script>
    <script src="{{ mix('js/app.js') }}" defer></script>
</body>
</html>
