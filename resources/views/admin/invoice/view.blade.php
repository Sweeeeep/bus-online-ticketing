@extends('admin.layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Invoice #{{ $invoice->invoice_no }}</h3>
        @if($invoice->status != 1)
        <div class="card-options">
            <form action="{{ route('admin.invoice.refund') }}" method="POST">
                @csrf
                <input type="hidden" name="id" value="{{ $invoice->id }}">
                <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to issue a refund?')">Issue Refund</button>
            </form>
        </div>
        @endif
    </div>
    @include('flash::message')
    <div class="card-body">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <table class="table">
                    <thead>
                        <tr>
                            <th colspan="2" class="text-center">Invoice Information</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td scope="row">Payment Method</td>
                            <td>
                                @if($invoice->type == 1)
                                    <i class="payment payment-{{ strtolower($invoice->card_brand) }}"></i> ~ {{ $invoice->card_last_four }}
                                @elseif($invoice->type == 2)
                                    <i class="payment payment-paypal"></i> ~ {{ $invoice->paypal_email }}
                                @else
                                    Cash
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td scope="row">Transaction ID</td>
                            <td>
                                {{ $invoice->transaction_id }}
                            </td>
                        </tr>
                        <tr>
                            <td scope="row">Amount</td>
                            <td>
                                {{ number_format($invoice->amount, 2) }}
                            </td>
                        </tr>
                        @if($invoice->status == 1)
                        <tr>
                            <td scope="row">Status</td>
                            <td class="text-danger">
                                <b>REFUNDED</b>
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
                <table class="table">
                    <thead>
                        <tr>
                            <th colspan="2" class="text-center">Customer Information</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td scope="row">Full Name</td>
                            <td>
                                {{ $invoice->customer->full_name }}
                            </td>
                        </tr>
                        <tr>
                            <td scope="row">Email</td>
                            <td>
                                {{ $invoice->customer->email }}
                            </td>
                        </tr>
                        <tr>
                            <td scope="row">Mobile Number</td>
                            <td>
                                {{ $invoice->customer->mobile_number }}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="table">
                    <thead>
                        <tr>
                            <th colspan="2" class="text-center">Ticket Information</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $ticket = $invoice->ticket;
                            $trip = $ticket->trip;
                            $routes = $trip->routes->where('type', 0)->first();
                            $terminal = $routes->terminal;

                        @endphp
                        <tr>
                            <td scope="row" width="40%">Ticket Number</td>
                            <td>
                                {{ $ticket->ticket_no }}
                            </td>
                        </tr>

                        <tr>
                            <td scope="row" width="40%">Boarding Point</td>
                            <td>
                                {{ optional($terminal)->name ?? '-' }}
                            </td>
                        </tr>
                        <tr>
                            <td scope="row" width="40%">Dropping Point</td>
                            <td>
                                {{ optional($invoice->ticket->route)->terminal->name ?? '-' }}
                            </td>
                        </tr>
                        <tr>
                            <td scope="row">Reserved Seats</td>
                            <td>
                                {{ implode($invoice->ticket->reserved_seats, ',') }}
                            </td>
                        </tr>
                        <tr>
                            <td scope="row">Amount</td>
                            <td>
                                PHP {{ number_format($invoice->ticket->amount, 2) }}
                            </td>
                        </tr>
                        <tr>
                            <td scope="row">Status</td>
                            <td>
                                {{ $ticket->ticket_status }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
