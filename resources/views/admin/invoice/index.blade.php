@extends('admin.layouts.admin')

@section('content')
<div class="card">
    <div class="card-body">
        <form>
            <div class="form-group mb-0">
                <div class="input-group">
                    <input type="text" class="form-control" name="q" placeholder="Search for invoice id">
                    <span class="input-group-append">
                        <button class="btn btn-primary" type="submit">Go!</button>
                    </span>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="card">
    @include('flash::message')
    <div class="table-responsive">
        <table class="table table-hover table-outline table-vcenter text-nowrap card-table">
            <thead>
                <tr>
                    <th class="text-center w-1"><i class="icon-people"></i></th>
                    <th>Customer</th>
                    <th>Invoice</th>
                    <th>Amount</th>
                    <th class="text-center">Payment Method</th>
                    <th class="text-center"><i class="fe fe-settings"></i></th>
                </tr>
            </thead>
            <tbody>
                @foreach($invoices as $invoice)
                <tr>
                    <td class="text-center">
                        <span class="avatar">{{ $invoice->customer->name_initials }}</span>
                    </td>
                    <td>
                        <div>{{ $invoice->customer->full_name }}</div>
                        <div class="small text-muted">
                            Created at: {{ $invoice->customer->created_at->format('M d, Y') }}
                        </div>
                    </td>
                    <td>
                        <div class="small text-muted">Invoice Number</div>
                        <div>{{ $invoice->invoice_no }}</div>
                    </td>
                    <td>
                        <div class="small text-muted">{{ count($invoice->ticket->reserved_seats) }} reserved ticket</div>
                        <div>PHP {{ number_format($invoice->amount, 2) }}</div>
                    </td>
                    <td class="text-center">
                        @if($invoice->type == 1)
                            <i class="payment payment-{{ strtolower($invoice->card_brand) }}"></i>
                        @elseif($invoice->type == 2)
                            <i class="payment payment-paypal"></i>
                        @else
                            Cash
                        @endif
                    </td>
                    <td class="text-center">
                        <a href="{{ route('admin.invoice.view', $invoice->id) }}" class="btn btn-secondary btn-sm">View</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
{{ $invoices->render()}}
@endsection
