@extends('admin.layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">List of Seat Layouts</h3>
    </div>
    @include('flash::message')
    <table class="table table-striped mb-0">
        <thead>
            <tr>
                <th scope="col">Liner</th>
                <th scope="col">Title</th>
                <th scope="col">Type</th>
                <th scope="col">Total Seats</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($seatLayouts as $seatLayout)
            <tr>
                <td>{{ optional($seatLayout->liner)->name }}</td>
                <td>{{ $seatLayout->title }}</td>
                <td>{{ BusHelpers::layoutType[$seatLayout->type] }}</td>
                <td>{{ $seatLayout->total_seats }}</td>
                <td>
                    <form action="{{ route('admin.seats.destroy', $seatLayout->id) }}" method="POST">
                        <a href="{{ route('admin.seats.edit', $seatLayout->id) }}" class="btn btn-secondary btn-sm">Update</a>
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete?')">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
{{ $seatLayouts->render() }}
@endsection
