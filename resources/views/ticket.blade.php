<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ticket Information #{{ $ticket->ticket_no }} - {{ config('app.name') }}</title>
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

</head>
<body>
    <div class="container my-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Ticket Information #{{ $ticket->ticket_no }}
                    </div>
                    <div class="card-body">
                        Issue Date: {{ $ticket->created_at->format('M d, Y')}}
                        <hr>
                        <div class="row">
                            <div class="col-md-3">
                                <b>Customer</b> <br>
                                {{ $customer->full_name }}
                            </div>
                            <div class="col-md-6">
                                <b>Passengers Name</b> <br>
                                @foreach($passengers as $passenger)
                                    {{ $passenger->name }}{{ !$loop->last ? ',' : ''}}
                                @endforeach
                            </div>
                            <div class="col-md-3">
                                <b>eTicket Number</b> <br>
                                {{ $ticket->ticket_no }}
                            </div>
                            <div class="col-md-12">
                                <hr>
                            </div>
                            <div class="col-md-12">
                                <h5><b>Ticket Information</b></h5>
                                <div class="row">
                                    <div class="col-md-6">
                                        <b>Bus Number</b> <br>
                                        {{ $trip->bus->body_number }}
                                    </div>
                                    <div class="col-md-6">
                                        <b>Seat Number</b> <br>
                                        <b>{!! implode($ticket->reserved_seats, '</b>, <b>') !!}</b>
                                    </div>
                                    <div class="col-md-6 mt-2">
                                        <b>Departure</b><br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <b>Date & Time</b><br>
                                                {{ $trip->depart_at->format('M d, Y @ g:i A') }}
                                            </div>
                                            <div class="col-md-6">
                                                <b>City</b><br>
                                                @php
                                                    $departure = $trip->routes->where('type', 0)->first();
                                                    $terminal = $departure->terminal;
                                                    $city = $terminal->city;
                                                @endphp
                                                {{ $city->name . ' (' .$terminal->name. ')' }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mt-2">
                                        <b>Arrival</b><br>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <b>Date & Time</b><br>
                                                {{ $trip->arrive_at->format('M d, Y @ g:i A') }}
                                            </div>
                                            <div class="col-md-6">
                                                <b>City</b><br>
                                                @php
                                                    $terminal = $route->terminal;
                                                    $city = $terminal->city;
                                                @endphp
                                                {{ $city->name . ' (' .$terminal->name. ')' }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p class="pt-3 mb-0">
                                    Bus operated by : {{ $trip->bus->liner->name }}
                                </p>
                            </div>
                            <div class="col-md-12">
                                <hr>
                            </div>
                            <div class="col-md-12">
                                @php
                                    $price = 0;
                                @endphp
                                <div class="row">
                                    <div class="col-md-6">
                                    <h5><b>Fare Information</b></h5>
                                        <table class="table table-sm">
                                            <tbody>
                                                @foreach($ticket->reserved_seats as $seat)
                                                    @php
                                                        $price+= ($trip->base_fare - $route->fare_deduct)
                                                    @endphp
                                                    <tr>
                                                        <td>Seat #{{ $seat }}</td>
                                                        <td class="text-right">
                                                            PHP {{ number_format($trip->base_fare - $route->fare_deduct, 2) }}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                <tr>
                                                    <td>Service Fee</td>
                                                    <td class="text-right">PHP {{ number_format((BusHelpers::serviceFee($price) - $price), 2) }}</td>
                                                </tr>
                                                <tr style="font-weight: bold;">
                                                    <td>Grand Total</td>
                                                    <td class="text-right">PHP {{ number_format((BusHelpers::serviceFee($price)), 2) }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <h5><b>Payment Information</b></h5>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <b>Invoice Number</b><br>
                                                {{ $invoice->invoice_no }}
                                            </div>
                                            <div class="col-md-12">
                                                <b>Payment Method</b><br>
                                                @if($invoice->type == 1)
                                                    <i class="fab fa-cc-{{ strtolower($invoice->card_brand) }}"></i> {{ $invoice->card_brand }} - {{ $invoice->card_last_four }}
                                                @elseif($invoice->type == 2)
                                                    <i class="fab fa-cc-paypal"></i> Paypal - {{ $invoice->paypal_email }}
                                                @else
                                                    <i class="fa fa-money-check-alt"></i> Cash
                                                @endif
                                            </div>
                                            <div class="col-md-12">
                                                <b>Amount Paid</b><br>
                                                PHP {{ number_format($ticket->amount, 2) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
