<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark bg-primary navbar-jhem navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <span class="d-block d-sm-none">
                        NCBSOT
                    </span>
                    <span class="d-none d-sm-block">
                        {{ config('app.name') }}
                    </span>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                    </ul>
                    <ul class="navbar-nav ml-auto">
                        @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('admin.index') }}">Admin Panel</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        @yield('top_content')
        <main class="container py-4 main-content">
            @yield('content')
        </main>
        <section id="footer">
            <div class="container">
                <div class="row text-center text-xs-center text-sm-left text-md-left">
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <h5>Top Bus Routes</h5>
                        <ul class="list-unstyled quick-links">
                            @foreach ($groupRoutes as $routes)
                                @php
                                    $boarding = '';
                                    $dropping = '';
                                    $trip;
                                    foreach ($routes->take(2) as $route) {
                                        $trip = $route->trip;
                                        if($route->terminal){
                                            if($route->terminal->city){
                                                $city = $route->terminal->city->name;
                                                if($route->type == 0){
                                                    $boarding = $city;
                                                }else{
                                                    $dropping = $city;
                                                }
                                            }
                                        }
                                    }
                                @endphp
                                @if($boarding && $dropping)
                                    <li>
                                        @php
                                            $url = '/trip/search?date='. Carbon\Carbon::now()->format('d/m/Y').'&seat_count=1&origin='.$boarding.'&destination='.$dropping;
                                        @endphp
                                        <a href="{!! $url !!}">
                                            <i class="fa fa-angle-double-right"></i>
                                            {!! $boarding. ' <i class="fa fa-arrow-right"></i> '. $dropping !!}
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <h5>Top Cities</h5>
                        <ul class="list-unstyled quick-links">
                            @foreach ($cities as $city)
                                <li>
                                    <a href="javascript:void();">
                                        <i class="fa fa-angle-double-right"></i>
                                        {{ $city->name }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <h5>Top Operator</h5>
                        <ul class="list-unstyled quick-links">
                            @foreach ($liners as $liner)
                                <li><a href="javascript:void();"><i class="fa fa-angle-double-right"></i>{{ $liner->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-5">
                        <ul class="list-unstyled list-inline social text-center">
                            <li class="list-inline-item"><a href="javascript:void();"><i class="fab fa-facebook"></i></a></li>
                            <li class="list-inline-item"><a href="javascript:void();"><i class="fab fa-twitter"></i></a></li>
                            <li class="list-inline-item"><a href="javascript:void();"><i class="fab fa-instagram"></i></a></li>
                            <li class="list-inline-item"><a href="javascript:void();"><i class="fab fa-google-plus"></i></a></li>
                            <li class="list-inline-item"><a href="javascript:void();" target="_blank"><i class="fa fa-envelope"></i></a></li>
                        </ul>
                    </div>
                    </hr>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 text-center">
                        <p class="h6">
                            <a class="text-green ml-2" href="{{ route('privacy') }}" target="_blank">Terms of Service</a> - <a class="text-green ml-2" href="{{ route('tos') }}" target="_blank">Privacy Policy</a>
                        </p>
                        <p class="h6">&copy All right Reversed.<a class="text-green ml-2" href="{{ url('/') }}" target="_blank">{{ config('app.name') }}</a></p>
                    </div>
                    </hr>
                </div>
            </div>
        </section>
    </div>
    <!-- Scripts -->
    <script src="https://js.stripe.com/v3/"></script>
    <script src="{{ mix('js/app.js') }}" defer></script>
</body>

</html>
