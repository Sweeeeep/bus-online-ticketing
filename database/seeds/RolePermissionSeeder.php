<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\Liner;
use App\User;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){

        $admin = Role::create(['name' => 'admin']);

        $liner = Role::create(['name' => 'tenant']);

        $admin = User::create([
            'name' => 'Site Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin123')
        ]);

        $admin->assignRole('admin');

    }
}
