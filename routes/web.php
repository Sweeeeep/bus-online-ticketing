<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('index');
})->name('index');

Route::view('/privacy', 'page.privacy')->name('privacy');
Route::view('/tos', 'page.tos')->name('tos');

Route::get('/contact', 'HomeController@contact')->name('contact');

Auth::routes();

Route::get('/checkout', 'TicketController@checkout');


Route::group(['prefix' => 'trip'], function () {
    Route::get('/search', 'TripController@search')->name('trip.search');

    Route::group(['prefix' => 'book'], function () {
        Route::get('/information', 'TicketController@book');
        Route::post('/checkout', 'TicketController@checkout');
        Route::get('/success/{id}', 'TicketController@success');
    });
});

Route::group(['prefix' => 'ticket'], function () {
    Route::get('/information/{ticket}/{checkout_id}', 'TicketController@information')->name('ticket.information');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'role:admin|tenant']], function () {
    Route::get('/', 'AdminController@index')->name('admin.index');

    Route::group(['prefix' => 'invoces'], function () {
        Route::get('/', 'InvoiceController@index')->name('admin.invoice.index');
        Route::get('/{id}', 'InvoiceController@view')->name('admin.invoice.view');

        Route::post('/issue/refund', 'InvoiceController@issueRefund')->name('admin.invoice.refund');
    });

    Route::group(['prefix' => 'tickets'], function () {
        Route::get('/', 'TicketController@index')->name('admin.ticket.index');

        Route::get('/create', 'TicketController@create')->name('admin.ticket.create');

        Route::get('/{id}', 'TicketController@view')->name('admin.ticket.view');
    });

    Route::group(['prefix' => 'users', 'middleware' => ['role:admin']], function () {
        Route::get('/', 'UserController@index')->name('admin.users.index');

        Route::get('/create', 'UserController@create')->name('admin.users.create');
        Route::post('/', 'UserController@store')->name('admin.users.store');

        Route::get('/{id}/edit', 'UserController@edit')->name('admin.users.edit');
        Route::post('/{id}', 'UserController@update')->name('admin.users.update');

        Route::delete('/{id}', 'UserController@destroy')->name('admin.users.destroy');
    });

    Route::group(['prefix' => 'buses'], function () {
        Route::get('/', 'BusController@index')->name('admin.buses.index');

        Route::get('/create', 'BusController@create')->name('admin.buses.create');
        Route::post('/', 'BusController@store')->name('admin.buses.store');

        Route::get('/{id}/edit', 'BusController@edit')->name('admin.buses.edit');
        Route::post('/{id}', 'BusController@update')->name('admin.buses.update');

        Route::delete('/{id}', 'BusController@destroy')->name('admin.buses.destroy');
    });

    Route::group(['prefix' => 'seats'], function () {
        Route::get('/', 'SeatLayoutController@index')->name('admin.seats.index');
        Route::get('/create', 'SeatLayoutController@create')->name('admin.seats.create');

        Route::get('/{id}/edit', 'SeatLayoutController@edit')->name('admin.seats.edit');

        Route::delete('/{id}', 'SeatLayoutController@destroy')->name('admin.seats.destroy');
    });

    Route::group(['prefix' => 'liners', 'middleware' => ['role:admin']], function () {
        Route::get('/', 'LinerController@index')->name('admin.liners.index');

        Route::get('/create', 'LinerController@create')->name('admin.liners.create');
        Route::post('/', 'LinerController@store')->name('admin.liners.store');

        Route::get('/{id}/edit', 'LinerController@edit')->name('admin.liners.edit');
        Route::post('/{id}', 'LinerController@update')->name('admin.liners.update');

        Route::delete('/{id}', 'LinerController@destroy')->name('admin.liners.destroy');
    });

    Route::group(['prefix' => 'trips'], function () {

        Route::post('/', 'TripController@store')->name('admin.trips.store');
        Route::get('/create', 'TripController@create')->name('admin.trips.create');

        Route::get('/{id}/edit', 'TripController@edit')->name('admin.trips.edit');
        Route::post('/{id}', 'TripController@update')->name('admin.trips.update');

        Route::delete('/{id}', 'TripController@destroy')->name('admin.trips.destroy');

        Route::get('/{type?}', 'TripController@index')->name('admin.trips.index');
    });

    Route::group(['prefix' => 'locations', 'middleware' => ['role:admin']], function () {
        Route::get('/', 'LocationController@index')->name('admin.locations.index');

        Route::namespace('Location')->group(function(){
            Route::group(['prefix' => 'province'], function () {

                Route::get('/create', 'ProvinceController@create')->name('admin.province.create');
                Route::post('/', 'ProvinceController@store')->name('admin.province.store');

                Route::get('/{id}/edit', 'ProvinceController@edit')->name('admin.province.edit');
                Route::post('/{id}', 'ProvinceController@update')->name('admin.province.update');

                Route::delete('/{id}', 'ProvinceController@destroy')->name('admin.province.destroy');

                Route::get('/{id}', 'ProvinceController@view')->name('admin.province.view');


                Route::get('/{provinceID}/city/create', 'CityController@create')->name('admin.city.create');
                Route::post('/{provinceID}/city/create', 'CityController@store')->name('admin.city.store');

                Route::get('/{provinceID}/city/{cityID}/edit', 'CityController@edit')->name('admin.city.edit');
                Route::post('/{provinceID}/city/{cityID}', 'CityController@update')->name('admin.city.update');
                Route::delete('/{provinceID}/city/{cityID}', 'CityController@destroy')->name('admin.city.destroy');



            });

            Route::group(['prefix' => 'city'], function () {
                Route::get('/{id}', 'CityController@view')->name('admin.city.view');

                Route::get('/{id}/terminal/create', 'TerminalController@create')->name('admin.terminal.create');

                Route::post('/{id}/terminal/create', 'TerminalController@store')->name('admin.terminal.store');

                Route::get('/{cityID}/terminal/{terminalID}/edit', 'TerminalController@edit')->name('admin.terminal.edit');

                Route::post('/{cityID}/terminal/{terminalID}', 'TerminalController@update')->name('admin.terminal.update');

                Route::delete('/{cityID}/terminal/{terminalID}', 'TerminalController@destroy')->name('admin.terminal.destroy');

            });
        });

    });

    Route::group(['prefix' => 'api'], function () {
        Route::group(['prefix' => 'seats'], function () {
            Route::post('/store', 'SeatLayoutController@store');
            Route::post('/get', 'SeatLayoutController@get');

            Route::post('/{id}', 'SeatLayoutController@update');
        });

        Route::group(['prefix' => 'liners'], function () {
            Route::post('/get', 'LinerController@get');
        });

        Route::group(['prefix' => 'buses'], function () {
            Route::post('/get', 'BusController@get');
        });

        Route::group(['prefix' => 'locations'], function () {
            Route::post('/terminals', 'LocationController@terminals');
            Route::post('/provinces', 'LocationController@provinces');
        });

        Route::group(['prefix' => 'trips'], function () {
            Route::post('/', 'TicketController@store');
            Route::post('/get', 'TripController@getById');
            Route::post('/search/{type?}', 'TripController@searchApi');
        });
    });
});

Route::group(['prefix' => 'api'], function () {
    Route::group(['prefix' => 'trips'], function () {
        Route::post('/search', 'TripController@searchApi');
    });
    Route::group(['prefix' => 'locations'], function () {
        Route::post('/provinces', 'LocationController@provinces');
    });
    Route::group(['prefix' => 'liners'], function () {
        Route::post('/', 'LinerController@getById');
    });
    Route::group(['prefix' => 'tickets'], function () {
        Route::post('/', 'TicketController@reservations');
        Route::post('/session', 'TicketController@reserveSession');
    });
});
