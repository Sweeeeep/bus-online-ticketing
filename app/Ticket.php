<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model{

    const PAID = 1;
    const PENDING = 0;
    const REFUNDED = 2;
    const CANCELLED = 3;

    protected $fillable = [
        'trip_id', 'customer_id', 'route_id', 'reserved_seats', 'amount', 'status'
    ];

    protected $appends = [
        'ticket_no'
    ];

    protected $casts = [
        'reserved_seats' => 'array'
    ];

    public function trip(){
        return $this->belongsTo(Trip::class);
    }

    public function customer(){
        return $this->belongsTo(Customer::class);
    }

    public function route(){
        return $this->belongsTo(Route::class);
    }

    public function invoice(){
        return $this->hasOne(Invoice::class);
    }

    public function passengers(){
        return $this->hasMany(Passenger::class);
    }

    public function getTicketNoAttribute() {
        return str_pad($this->id, 10, '0', STR_PAD_LEFT);
    }

    public function getTicketStatusAttribute(){
        if($this->status == self::PENDING){
            return 'Payment Pending';
        }elseif($this->status == self::REFUNDED){
            return 'Refunded';
        }else{
            return 'Paid';
        }
    }

}
