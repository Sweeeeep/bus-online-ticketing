<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;

class Customer extends Model{

    use Notifiable;
    use Billable;

    protected $fillable = [
        'first_name', 'last_name', 'mobile_number', 'email'
    ];


    public function getFullNameAttribute(){
        return $this->first_name. ' ' . $this->last_name;
    }

    public function getNameInitialsAttribute(){
        return $this->first_name[0].$this->last_name[0];
    }

    public function getPhoneNumberAttribute(){
        return '63'.substr($this->mobile_number, 1);
    }

}
