<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Cashier\Cashier;
use App\Liner;
use App\Route;
use Illuminate\Support\Facades\DB;
use App\Location\City;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Cashier::useCurrency('php', '₱');
        view()->composer('layouts.app', function ($view) {
            $liners = Liner::take(5)->get();
            $view->with('liners', $liners);

            $cities = City::take(5)->get();
            $view->with('cities', $cities);

            $routes = Route::with('trip', 'terminal.city')->get();
            $routes = $routes->groupBy('trip_id')->take(5);

            $view->with('groupRoutes', $routes);
        });
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
