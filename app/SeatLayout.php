<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;


class SeatLayout extends Model{

    protected $casts = [
        'seat_map' => 'array'
    ];

    protected static function boot(){
        parent::boot();

        static::addGlobalScope('liner', function (Builder $builder) {
            if(auth()->check()){
                $user = auth()->user();
                if(!$user->hasRole('admin')){
                    $builder->where('liner_id', $user->liner_id);
                }
            }
        });

        static::created(function($seatLayout){
            if(auth()->check()){
                $user = auth()->user();
                if(!$user->hasRole('admin')){
                    $seatLayout->liner_id = $user->liner_id;
                    $seatLayout->save();
                }
            }
        });
    }

    public function liner(){
        return $this->belongsTo(Liner::class);
    }

}
