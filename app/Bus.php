<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Bus extends Model{

    protected $fillable = [
        'liner_id', 'seat_layout_id', 'name', 'body_number', 'seat_capacity', 'amenities', 'type'
    ];

    protected $casts = [
        'amenities' => 'array'
    ];

    protected static function boot(){
        parent::boot();

        static::addGlobalScope('liner', function(Builder $builder){
            if(auth()->check()){
                $user = auth()->user();
                if(!$user->hasRole('admin')){
                    $builder->where('liner_id', $user->liner_id);
                }
            }
        });

        static::created(function($bus){
            if(auth()->check()){
                $user = auth()->user();
                if(!$user->hasRole('admin')){
                    $bus->liner_id = $user->liner_id;
                    $bus->save();
                }
            }
        });
    }

    public function liner(){
        return $this->belongsTo(Liner::class);
    }

    public function seatlayout(){
        return $this->belongsTo(SeatLayout::class, 'seat_layout_id');
    }

    public function trips(){
        return $this->hasMany(Trip::cass);
    }

}
