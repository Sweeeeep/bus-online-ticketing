<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model{

    const ACTIVE = 0;
    const REFUNDED = 1;

    protected $fillable = [
        'ticket_id', 'customer_id', 'amount', 'type', 'stripe_id', 'card_brand', 'card_last_four', 'paypal_email', 'transaction_id'
    ];

    protected $appends = [
        'invoice_no'
    ];

    public function customer(){
        return $this->belongsTo(Customer::class);
    }

    public function ticket(){
        return $this->belongsTo(Ticket::class);
    }

    public function getInvoiceNoAttribute() {
        return str_pad($this->id, 10, '0', STR_PAD_LEFT);
    }

}
