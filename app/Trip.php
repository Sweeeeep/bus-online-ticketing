<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Trip extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'base_fare', 'arrive_at', 'depart_at', 'bus_id'
    ];

    protected $dates = [
        'arrive_at', 'depart_at', 'deleted_at'
    ];

    protected $appends = [
        'available_seats'
    ];

    protected static function boot(){
        parent::boot();

        // static::addGlobalScope('status', function(Builder $builder){
        //     $builder->where('depart_at', '>=', Carbon::now());
        // });
    }

    public function bus(){
        return $this->belongsTo(Bus::class);
    }

    public function busNoScope(){
        return $this->belongsTo(Bus::class, 'bus_id', 'id')->withoutGlobalScopes();
    }

    public function routes(){
        return $this->hasMany(Route::class);
    }

    public function tickets(){
        return $this->hasMany(Ticket::class);
    }

    public function departure(){
        return $this->routes
                ->where('type', 0)->first();
    }

    public function getAvailableSeatsAttribute(){
        $bus = $this->bus()->withoutGlobalScopes()->first();
        $tickets = $this->tickets()->whereNotIn('status', [Ticket::REFUNDED, Ticket::CANCELLED])->get();
        $count = 0;
        foreach ($tickets as $key => $ticket) {
            $count+= count($ticket->reserved_seats);
        }

        return $bus->seat_capacity - $count;

    }
}
