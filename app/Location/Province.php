<?php

namespace App\Location;

use Illuminate\Database\Eloquent\Model;

class Province extends Model{

    protected $fillable = [
        'name'
    ];

    public function cities(){
        return $this->hasMany(City::class);
    }

    public function terminals(){
        return $this->hasMany(Terminal::class);
    }
}
