<?php

namespace App\Location;

use Illuminate\Database\Eloquent\Model;

class Terminal extends Model{

    protected $fillable = [
        'city_id', 'province_id', 'name'
    ];

    public function province(){
        return $this->belongsTo(Province::class);
    }

    public function city(){
        return $this->belongsTo(City::class);
    }
}
