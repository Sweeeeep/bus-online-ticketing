<?php

namespace App\Location;

use Illuminate\Database\Eloquent\Model;

class City extends Model{

    protected $fillable = [
        'province_id', 'name'
    ];

    public function province(){
        return $this->belongsTo(Province::class);
    }

    public function terminals(){
        return $this->hasMany(Terminal::class);
    }
}
