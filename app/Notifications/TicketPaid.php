<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\NexmoMessage;

class TicketPaid extends Notification
{
    use Queueable;

    public $invoice;
    public $ticket;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($invoice, $ticket)
    {
        $this->invoice = $invoice;
        $this->ticket = $ticket;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['nexmo'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    public function toNexmo($notifiable){
        return (new NexmoMessage)->content('Hi '.$this->invoice->customer->last_name.', Thank you for using our '.config('app.name').'. Your Ticket #'.$this->ticket->ticket_no.' and Invoice #'.$this->invoice->invoice_no)->from(config('services.nexmo.sms_from'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
