<?php

namespace App\Observers;

use App\SeatLayout;

class SeatLayoutObserver
{
    /**
     * Handle the seat layout "created" event.
     *
     * @param  \App\SeatLayout  $seatLayout
     * @return void
     */
    public function created(SeatLayout $seatLayout){
        if(auth()->check()){
            $user = auth()->user();
            if(!$user->hasRole('admin')){
                $seatLayout->liner_id = $user->liner_id;
                $seatLayout->save();
            }
        }
    }

    /**
     * Handle the seat layout "updated" event.
     *
     * @param  \App\SeatLayout  $seatLayout
     * @return void
     */
    public function updated(SeatLayout $seatLayout)
    {
        //
    }

    /**
     * Handle the seat layout "deleted" event.
     *
     * @param  \App\SeatLayout  $seatLayout
     * @return void
     */
    public function deleted(SeatLayout $seatLayout)
    {
        //
    }

    /**
     * Handle the seat layout "restored" event.
     *
     * @param  \App\SeatLayout  $seatLayout
     * @return void
     */
    public function restored(SeatLayout $seatLayout)
    {
        //
    }

    /**
     * Handle the seat layout "force deleted" event.
     *
     * @param  \App\SeatLayout  $seatLayout
     * @return void
     */
    public function forceDeleted(SeatLayout $seatLayout)
    {
        //
    }
}
