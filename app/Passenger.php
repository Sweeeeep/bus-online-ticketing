<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Passenger extends Model{

    protected $fillable = [
        'name', 'seat_no', 'ticket_id',
    ];

    public function ticket(){
        return $this->belongsTo(Ticket::class);
    }
}
