<?php

use Carbon\Carbon;

class BusHelpers{

    const Amenities = [
        'WIFI', 'TV', 'Restroom', 'Charging Port', 'Personal TV', 'Pillow'
    ];

    const BusType = [
        'AC', 'Non AC', 'Sleeper', 'Semi Sleeper'
    ];

    const layoutType = [
        '1x1', '2x1', '2x2', '2x3'
    ];

    public static function mergeDateTime($date, $time){
        $d = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
        $t = Carbon::parse($time)->format('H:i:s');

        return Carbon::createFromTimestamp(strtotime($d . $t));
    }

    public static function serviceFee($amount = 0){
        return ($amount + .30) / (1 - .029);
    }

    public static function toCents($amount){
        return bcmul($amount, 100);
    }

    public static function isActiveRoute($route, $output = "active"){
        if (Route::currentRouteName() == $route) return $output;
    }

    public static function areActiveRoutes(Array $routes, $output = "active"){
        foreach ($routes as $route){
            if (Route::currentRouteName() == $route) return $output;
        }

    }


}
