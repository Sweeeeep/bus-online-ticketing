<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Liner extends Model{

    protected $appends = ['logo_url'];

    public function buses(){
        return $this->hasMany(Bus::class);
    }

    public function getLogoUrlAttribute(){
        return asset('images/liners/'.$this->logo);
    }

    public function trips(){
        return $this->hasMany(Trip::class);
    }

}
