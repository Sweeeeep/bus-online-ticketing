<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Liner;

class LinerController extends Controller{

    public function index(){
        $liners = Liner::withCount('buses')->paginate(10);

        return view('admin.liners.index', compact('liners'));
    }

    public function create(){
        return view('admin.liners.create');
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required',
            'mobile_number' => 'required|numeric|min:11',
            'logo' => 'required|image',
            'address' => 'required',
        ]);

        $file = $request->logo;

        if(!$file->isValid()){
            flash('Invalid Uploaded Logo')->error();
            return redirect()->route('admin.liners.create');
        }

        $fileInfo = pathinfo($file->getClientOriginalName());

        $fileName = md5($fileInfo['filename']).'.'.$fileInfo['extension'];

        $file->storeAs('liners', $fileName, 'images');

        $liner = new Liner;
        $liner->name = $request->name;
        $liner->mobile_number = $request->mobile_number;
        $liner->address = $request->address;
        $liner->logo = $fileName;
        $liner->save();

        flash('Successfully Added.')->success();

        return redirect()->route('admin.liners.create');

    }

    public function edit($id){
        $liner = Liner::where('id', $id)->firstOrFail();

        return view('admin.liners.edit', compact('liner'));
    }

    public function update(Request $request){

        $request->validate([
            'name' => 'required',
            'mobile_number' => 'required|numeric|min:11',
            'logo' => 'image',
            'address' => 'required',
        ]);

        $liner = Liner::where('id', $request->id)->firstOrFail();

        if($request->logo){
            $file = $request->logo;

            if(!$file->isValid()){
                flash('Invalid Uploaded Logo')->error();
                return redirect()->route('admin.liners.create');
            }

            $fileInfo = pathinfo($file->getClientOriginalName());

            $fileName = md5($fileInfo['filename']).'.'.$fileInfo['extension'];

            $file->storeAs('liners', $fileName, 'images');

            $liner->logo = $fileName;
        }

        $liner->name = $request->name;
        $liner->mobile_number = $request->mobile_number;
        $liner->address = $request->address;
        $liner->save();

        flash('Successfully Updated.')->success();

        return redirect()->route('admin.liners.edit', $liner->id);

    }

    public function destroy($id){
        $liner = Liner::where('id', $id)->firstOrFail();
        $liner->delete();

        flash('Successfully Deleted.')->success();
        return redirect()->route('admin.liners.index');

    }

    //API

    public function get(){
        $liners = Liner::all();

        return response()->json([
            'status' => true,
            'results' => $liners
        ]);
    }

    public function getById(Request $request){

        $liner = Liner::where('id', $request->id)->first();

        if(!$liner){
            return response()->json([
                'status' => false
            ]);
        }

        return response()->json([
            'status' => true,
            'result' => $liner
        ]);

    }

}
