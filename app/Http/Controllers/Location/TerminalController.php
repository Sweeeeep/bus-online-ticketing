<?php

namespace App\Http\Controllers\Location;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Location\Terminal;
use App\Location\City;

class TerminalController extends Controller{

    public function create($id){
        $city = City::where('id', $id)->firstOrFail();

        return view('admin.location.terminal.create', compact('city'));
    }

    public function store($id, Request $request){

        $city = City::where('id', $id)->firstOrFail();

        $request->validate([
            'name' => 'required',
        ]);

        $terminal = Terminal::create([
            'city_id' => $city->id,
            'province_id' => $city->province_id,
            'name' => $request->name
        ]);

        flash('Successfully Added')->success();

        return redirect()->route('admin.city.view', $city->id);

    }

    public function edit($cityID, $terminalID){
        $terminal = Terminal::where('id', $terminalID)->where('city_id', $cityID)->firstOrFail();

        return view('admin.location.terminal.edit', compact('terminal'));
    }

    public function update($cityID, $terminalID, Request $request){

        $terminal = Terminal::where('id', $terminalID)->where('city_id', $cityID)->firstOrFail();

        $request->validate([
            'name' => 'required'
        ]);

        $terminal->name = $request->name;
        $terminal->save();

        flash('Successfully Updated')->success();

        return redirect()->route('admin.city.view', $terminal->city_id);

    }

    public function destroy($cityID, $terminalID){
        $terminal = Terminal::where('id', $terminalID)->where('city_id', $cityID)->firstOrFail();
        $terminal->delete();

        flash('Successfully Deleted')->success();
        return redirect()->route('admin.city.view', $cityID);

    }

}
