<?php

namespace App\Http\Controllers\Location;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Location\Province;

class ProvinceController extends Controller{

    public function view($id){
        $province = Province::where('id', $id)->with(['cities' => function($q){
            $q->withCount('terminals');
        }])->firstOrFail();

        return view('admin.location.province.view', compact('province'));
    }

    public function create(){
        return view('admin.location.province.create');
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required'
        ]);

        $province = Province::create([
            'name' => $request->name
        ]);

        flash('Successfully added')->success();
        return redirect()->route('admin.locations.index');

    }

    public function edit($id){
        $province = Province::where('id', $id)->firstOrFail();


        return view('admin.location.province.edit', compact('province'));

    }

    public function update($id, Request $request){
        $province = Province::where('id', $id)->firstOrFail();

        $request->validate([
            'name' => 'required'
        ]);

        $province->name = $request->name;
        $province->save();

        flash('Successfully updated')->success();

        return redirect()->route('admin.locations.index');
    }

    public function destroy($id){
        $province = Province::where('id', $id)->firstOrFail();
        $province->delete();

        flash('Successfully deleted')->success();
        return redirect()->route('admin.locations.index');

    }
}
