<?php

namespace App\Http\Controllers\Location;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Location\City;
use App\Location\Terminal;
use App\Location\Province;

class CityController extends Controller{

    public function view($id){

        $city = City::where('id', $id)->firstOrFail();

        return view('admin.location.city.view', compact('city'));

    }

    public function edit($provinceID, $cityID){
        $city = City::where('id', $cityID)->where('province_id', $provinceID)->firstOrFail();

        return view('admin.location.city.edit', compact('city'));
    }

    public function update($provinceID, $cityID, Request $request){
        $city = City::where('id', $cityID)->where('province_id', $provinceID)->firstOrFail();

        $request->validate([
            'name' => 'required'
        ]);

        $city->name = $request->name;
        $city->save();

        flash('Successfully Updated')->success();

        return redirect()->route('admin.province.view', $city->province_id);
    }

    public function create($id){
        $province = Province::where('id', $id)->firstOrFail();

        return view('admin.location.city.create', compact('province'));
    }

    public function store($id, Request $request){

        $province = Province::where('id', $id)->firstOrFail();

        $request->validate([
            'name' => 'required',
        ]);

        $city = City::create([
            'province_id' => $province->id,
            'name' => $request->name
        ]);

        flash('Successfully Added')->success();

        return redirect()->route('admin.province.view', $province->id);
    }

    public function destroy($provinceID, $cityID){

        $city = City::where('id', $cityID)->where('province_id', $provinceID)->firstOrFail();
        $city->delete();

        flash('Successfully Deleted')->success();
        return redirect()->route('admin.province.view', $provinceID);

    }
}
