<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SeatLayout;

use Validator;

class SeatLayoutController extends Controller{

    public function index(){
        $seatLayouts = SeatLayout::with('liner')->paginate(10);

        return view('admin.seats.index', compact('seatLayouts'));
    }

    public function create(){
        return view('admin.seats.create');
    }

    public function store(Request $request){
        if(!$request->user()){
            return response()->json([
                'status' => false,
                'message' => 'Unauthorized'
            ]);
        }

        $user = $request->user();


        $validator = Validator::make($request->all(), [
            'map' => 'required|array',
            'liner' => 'required',
            'title' => 'required',
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'errors'=> $validator->errors()
            ]);
        }

        if(!is_array($request->map)){
            return response()->json([
                'status' => false,
                'message' => 'Invalid Format of Seat Map'
            ]);
        }

        $total_seats = 0;
        foreach($request->map as $rows){
            $seats = str_split($rows);
            foreach($seats as $seat){
                if($seat != '_' || $seat != 'C'){
                    $total_seats++;
                }
            }
        }

        $layout = new SeatLayout;
        $layout->liner_id = $user->liner_id ? $user->liner_id : $request->liner;
        $layout->title = $request->title;
        $layout->seat_map = $request->map;
        $layout->type = $request->type;
        $layout->total_seats = $total_seats;
        $layout->save();

        return response()->json([
            'status' => true,
            'message' => 'Successfully addded'
        ]);

    }

    public function edit($id){
        $seatLayout = SeatLayout::where('id', $id)->firstOrFail();


        return view('admin.seats.edit', compact('seatLayout'));
    }

    public function update($id, Request $request){
        if(!$request->user()){
            return response()->json([
                'status' => false,
                'message' => 'Unauthorized'
            ]);
        }

        $user = $request->user();

        if(!is_array($request->map)){
            return response()->json([
                'status' => false,
                'message' => 'Invalid Format of Seat Map'
            ]);
        }

        $seatLayout = SeatLayout::where('id', $request->id)->firstOrFail();

        if(!$seatLayout){
            return response()->json([
                'status' => false,
                'message' => 'Unable to find Seat Layout #'.$request->id
            ]);
        }

        $total_seats = 0;
        foreach($request->map as $rows){
            $seats = str_split($rows);
            foreach($seats as $seat){
                if($seat == 'D' || $seat == 'R'){
                    $total_seats++;
                }
            }
        }

        $seatLayout->liner_id = $user->liner_id ? $user->liner_id : $request->liner;
        $seatLayout->title = $request->title;
        $seatLayout->seat_map = $request->map;
        $seatLayout->type = $request->type;
        $seatLayout->total_seats = $total_seats;
        $seatLayout->save();

        return response()->json([
            'status' => true,
            'message' => 'Successfully updated'
        ]);

    }

    public function destroy($id){
        $seatLayout = SeatLayout::where('id', $id)->firstOrFail();

        $seatLayout->delete();

        flash('Successfully Deleted')->success();
        return redirect()->route('admin.seats.index');
    }

    //api

    public function get(Request $request){
        $seatLayout = SeatLayout::where('id', $request->id)->firstOrFail();

        return response()->json([
            'status' => true,
            'results' => $seatLayout
        ]);

    }
}
