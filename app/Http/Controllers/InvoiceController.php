<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice;

use Stripe\Refund;
use Srmklive\PayPal\Services\ExpressCheckout;
use App\Ticket;
use App\Notifications\InvoiceRefund;

class InvoiceController extends Controller{

    public function index(Request $request){
        $invoices = Invoice::with('customer', 'ticket')->latest();

        $user = auth()->user();
        if(!$user->hasRole('admin')){
            $invoices = $invoices->whereHas('ticket', function($q) use($user){
                $q->whereHas('trip', function($q) use($user){
                    $q->whereHas('busNoScope', function($q) use($user){
                        $q->where('liner_id', $user->liner_id);
                    });
                });
            });
        }

        if(isset($request->q)){
            $invoices->where('id', (int)$request->q);
        }

        $invoices = $invoices->paginate(10);

        return view('admin.invoice.index', compact('invoices'));
    }

    public function view($id){
        $invoice = Invoice::with('customer', 'ticket')->where('id', $id)->firstOrFail();

        return view('admin.invoice.view', compact('invoice'));
    }

    public function issueRefund(Request $request){
        $invoice = Invoice::where('id', $request->id)->where('status', Invoice::ACTIVE)->first();

        if(!$invoice){
            flash('Invalid Invoice ID')->error();
            return redirect()->route('admin.invoice.index');
        }

        if($invoice->type == 1){
            $customer = $invoice->customer;
            try{
                $response = $customer->refund($invoice->transaction_id);
            }catch(Exception $e){
                flash('Unable to refund the invoice #'.$invoice->invoice_no.' please try again later');
                return redirect()->route('admin.invoice.view', $invoice->id);
            }

            if($response->status == 'succeeded'){
                $invoice->status = Invoice::REFUNDED;
                $invoice->ticket->status = Ticket::REFUNDED;
                $invoice->ticket->save();
                $customer->notify(new InvoiceRefund($invoice));
            }else{
                flash('Unable to refund the invoice #'.$invoice->invoice_no.' as this moment');
                return redirect()->route('admin.invoice.view', $invoice->id);
            }
            $invoice->save();
        }elseif($invoice->type == 2){
            $provider = new ExpressCheckout;
            $response = $provider->refundTransaction($invoice->transaction_id);

            if($response['ACK'] == 'Success'){
                $invoice->status = Invoice::REFUNDED;
                $invoice->ticket->status = Ticket::REFUNDED;
                $invoice->ticket->save();
                $invoice->customer->notify(new InvoiceRefund($invoice));
            }else{
                flash('Unable to refund the invoice #'.$invoice->invoice_no.' as this moment');
                return redirect()->route('admin.invoice.view', $invoice->id);
            }
            $invoice->save();
        }else{
            $invoice->status = Invoice::REFUNDED;
            $invoice->ticket->status = Ticket::REFUNDED;
            $invoice->ticket->save();
            $invoice->save();
        }
        flash('Successfully refunded the invoice #'.$invoice->invoice_no);
        return redirect()->route('admin.invoice.view', $invoice->id);
    }

}
