<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trip;
use App\Invoice;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Ticket;

class AdminController extends Controller{
    public function index(){

        $user = auth()->user();

        $activeTrips = Trip::whereHas('busNoScope', function($q) use($user){
            if(!$user->hasRole('admin')){
                $q->where('liner_id' , $user->liner_id);
            }
        })->whereDate('depart_at', Carbon::now()->toDateString())->count();
        $closeTrip = Trip::whereHas('busNoScope', function($q) use($user){
            if(!$user->hasRole('admin')){
                $q->where('liner_id' , $user->liner_id);
            }
        })->where('depart_at', '>', Carbon::now())->count();

        $dailyEarnings = Invoice::whereHas('ticket', function($q) use($user){
            if(!$user->hasRole('admin')){
                $q->whereHas('trip', function($q) use($user){
                    $q->whereHas('busNoScope', function($q) use($user){
                        $q->where('liner_id', $user->liner_id);
                    });
                });
            }
        })->whereDate('created_at', Carbon::now()->toDateString())->select(DB::raw('sum(amount) as total_amount'))->first();

        $ticketPaid = Ticket::whereHas('trip', function($q) use($user){
            if(!$user->hasRole('admin')){
                $q->whereHas('busNoScope', function($q) use($user){
                    $q->where('liner_id', $user->liner_id);
                });
            }
        })->whereDate('created_at', Carbon::now()->toDateString())->where('status', Ticket::PAID)->count();
        $ticketPending = Ticket::whereHas('trip', function($q) use($user){
            if(!$user->hasRole('admin')){
                $q->whereHas('busNoScope', function($q) use($user){
                    $q->where('liner_id', $user->liner_id);
                });
            }
        })->whereDate('created_at', Carbon::now()->toDateString())->where('status', Ticket::PENDING)->count();

        $invoices = Invoice::with('customer', 'ticket')->whereHas('ticket', function($q) use($user){
            if(!$user->hasRole('admin')){
                $q->whereHas('trip', function($q) use($user){
                    $q->whereHas('busNoScope', function($q) use($user){
                        $q->where('liner_id', $user->liner_id);
                    });
                });
            }
        })->latest()->paginate(10);

        return view('admin.index', compact('invoices', 'activeTrips','closeTrip', 'dailyEarnings', 'ticketPaid', 'ticketPending'));
    }
}
