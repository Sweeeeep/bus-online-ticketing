<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bus;
use App\Trip;
use Carbon\Carbon;

use BusHelpers;
use App\Route;
use App\Location\Terminal;
use App\Location\City;

use App\Http\Resources\Trip as TripResource;
use App\Ticket;

class TripController extends Controller{

    public function index($type = 'active'){
        $trips = Trip::with(['routes.terminal.city', 'bus.seatlayout']);

        $user = auth()->user();

        if($user->hasRole('tenant')){
            $trips = $trips->whereHas('busNoScope', function($q) use($user){
                $q->where('liner_id', $user->liner_id);
            });
        }

        if($type == 'archives'){
            $trips = $trips->where('depart_at', '<', Carbon::now());
        }else{
            $trips = $trips->where('depart_at', '>', Carbon::now());
        }

        $trips = $trips->paginate(10);

        return view('admin.trip.index', compact('trips'));
    }

    public function create(){
        return view('admin.trip.create');
    }

    public function store(Request $request){

        if(!$request->user()){
            abort(403);
        }

        $user = $request->user();

        $rules = [
            'bus' => 'required|numeric',
            'departure.date' => 'required|date_format:"d/m/Y"',
            'departure.time' => 'required',
            'arrival.date' => 'required|date_format:d/m/Y',
            'arrival.time' => 'required',
            'base_fare' => 'required|numeric',
            'boardingPoint' => 'required|array',
            'droppingPoints' => 'required|array',
        ];
        $messages = [
            'departure.date.required' => 'The departure date field is required',
            'departure.time.required' => 'The departure time field is required',
            'arrival.date.required' => 'The arrival date field is required',
            'arrival.time.required' => 'The arrival time field is required',
            'arrival.date.after' => 'The arrival must be a date after departure.'
        ];
        foreach($request->droppingPoints as $key => $bPoint){
            $rules['droppingPoints.'. $key . '.location'] = 'required|array';
            $messages['droppingPoints.'.$key.'.location.required'] = 'The '.( $key == 0 ? 'destination' : 'dropping').' point field is required.';
        }

        \Validator::make($request->all(), $rules, $messages)->validate();

        $bus = Bus::where('id', $request->bus)->first();


        if(!$bus){
            return response()->json([
                'status' => false,
                'message' => 'Invalid Bus ID'
            ]);
        }

        $arriveDT = BusHelpers::mergeDateTime($request->arrival['date'], $request->arrival['time']);
        $departDT = BusHelpers::mergeDateTime($request->departure['date'], $request->departure['time']);

        if($arriveDT->isPast()){
            return response()->json([
                'status' => false,
                'message' => 'Departure Date is past.'
            ]);
        }

        if($departDT->isPast()){
            return response()->json([
                'status' => false,
                'message' => 'Departure Date is past'
            ]);
        }

        if($departDT->greaterThan($arriveDT)){
            return response()->json([
                'status' => false,
                'message' => 'The arrival must be a date after departure'
            ]);
        }

        $check = Trip::where('bus_id', $bus->id)
                ->where('arrive_at', '>=', $departDT)
                ->where('depart_at', '<=', $arriveDT)->first();

        if($check){
            return response()->json([
                'status' => false,
                'message' => 'Bus is currently have active trip'
            ]);
        }

        $trip = Trip::create([
            'bus_id' => $bus->id,
            'base_fare' => $request->base_fare,
            'arrive_at' => $arriveDT,
            'depart_at' => $departDT
        ]);

        $boardingPoint = head($request->boardingPoint);
        $bTerminal = Terminal::where('id', $boardingPoint['id'])->first();
        Route::create([
            'terminal_id' => $boardingPoint['id'],
            'city_id' => $bTerminal->city_id,
            'trip_id' => $trip->id,
            'fare_deduct' => 0,
            'sequence' => 0,
            'type' => 0
        ]);

        foreach ($request->droppingPoints as $key => $value) {
            $location = head($value['location']);
            $terminal = Terminal::where('id', $location['id'])->first();
            Route::create([
                'terminal_id' => $location['id'],
                'city_id' => $terminal->city_id,
                'trip_id' => $trip->id,
                'fare_deduct' => $value['fare_deduct'] ?? 0,
                'sequence' => ($key + 1),
                'type' => 1
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 'Success'
        ]);

    }

    public function edit($id){
        return view('admin.trip.edit', compact('id'));
    }

    public function update(Request $request){

        if(!$request->user()){
            abort(403);
        }

        $user = $request->user();

        $rules = [
            'bus' => 'required|numeric',
            'departure.date' => 'required|date_format:"d/m/Y"',
            'departure.time' => 'required',
            'arrival.date' => 'required|date_format:d/m/Y',
            'arrival.time' => 'required',
            'base_fare' => 'required|numeric',
            'boardingPoint' => 'required|array',
            'droppingPoints' => 'required|array',
        ];
        $messages = [
            'departure.date.required' => 'The departure date field is required',
            'departure.time.required' => 'The departure time field is required',
            'arrival.date.required' => 'The arrival date field is required',
            'arrival.time.required' => 'The arrival time field is required',
            'arrival.date.after' => 'The arrival must be a date after departure.'
        ];
        foreach($request->droppingPoints as $key => $bPoint){
            $rules['droppingPoints.'. $key . '.location'] = 'required|array';
            $messages['droppingPoints.'.$key.'.location.required'] = 'The '.( $key == 0 ? 'destination' : 'dropping').' point field is required.';
        }

        \Validator::make($request->all(), $rules, $messages)->validate();

        $trip = Trip::where('id', $request->id)->with('routes')->first();

        if(!$trip){
            return response()->json([
                'status' => false,
                'message' => 'Unable to find the Trip you want to update'
            ]);
        }

        $bus = Bus::where('id', $request->bus)->first();

        if(!$bus){
            return response()->json([
                'status' => false,
                'message' => 'Invalid Bus ID'
            ]);
        }

        $arriveDT = BusHelpers::mergeDateTime($request->arrival['date'], $request->arrival['time']);
        $departDT = BusHelpers::mergeDateTime($request->departure['date'], $request->departure['time']);

        if($trip->bus_id != $request->bus){
            $check = Trip::where('bus_id', $bus->id)
                ->where('arrive_at', '>=', $departDT)
                ->where('depart_at', '<=', $arriveDT)
                ->first();

            if($check){
                return response()->json([
                    'status' => false,
                    'message' => 'Bus is occupied at that schedule'
                ]);
            }
        }

        $trip->bus_id = $request->bus;
        $trip->base_fare = $request->base_fare;
        $trip->arrive_at = $arriveDT;
        $trip->depart_at = $departDT;
        $trip->save();

        $boardingPoint = array_first($request->boardingPoint);
        $boardingRoute = $trip->routes->first();

        $boardingRoute->terminal_id = $boardingPoint['id'];
        $boardingRoute->type = 0;
        $boardingRoute->save();

        foreach ($request->droppingPoints as $dKey => $dPoint) {
            $routes = array_pluck($request->droppingPoints, 'route_id');
            $location = array_first($dPoint['location']);
            $terminal = Terminal::where('id', $location['id'])->first();
            if(!isset($dPoint['route_id'])){
                $route = Route::create([
                    'terminal_id' => $location['id'],
                    'city_id' => $terminal->city_id,
                    'trip_id' => $trip->id,
                    'fare_deduct' => $dPoint['fare_deduct'] ?? 0,
                    'sequence' => ($dKey + 1),
                    'type' => 1
                ]);
            }else{
                foreach ($trip->routes->where('type', 1) as $key => $route) {
                    $dTerminal = Terminal::where('id', $route->terminal_id)->first();
                    if($route->id == $dPoint['route_id']){
                        $route->terminal_id = $location['id'];
                        $route->city_id = $dTerminal->city_id;
                        $route->fare_deduct = $dPoint['fare_deduct'] ?? 0;
                        $route->sequence = $key;
                        $route->save();
                    }else{
                        $route->delete();
                    }
                }
            }

        }

        return response()->json([
            'status' => true,
            'message' => 'Success'
        ]);
    }

    public function destroy($id){
        $trip = Trip::where('id', $id)->firstOrFail();

        $trip->routes()->delete();
        $trip->delete();

        flash('Successfully Deleted')->success();
        return redirect()->route('admin.trips.index');
    }

    // API
    public function getById(Request $request){
        $trip = Trip::where('id', $request->id)
                    ->where('depart_at', '>', Carbon::now())
                    ->with(['routes.terminal.city', 'bus.seatlayout'])
                    ->first();

        if(!$trip){
            return response()->json([
                'status' => false,
                'message' => 'Unable to find the trip'
            ]);
        }

        $boardingPoint = $trip->routes->first();

        $droppingPoints = [];
        foreach($trip->routes->where('type', 1) as $drop){
            $droppingPoints[] = [
                'route_id' => $drop->id,
                'location' => [$drop->terminal->only('id', 'name')],
                'fare_deduct' =>  $drop->fare_deduct
            ];
        }

        $results = [
            'bus' => $trip->bus_id,
            'departure' => [
                'date' => $trip->depart_at->format('d/m/Y'),
                'time' => $trip->depart_at->format('g:i A')
            ],
            'arrival' => [
                'date' => $trip->arrive_at->format('d/m/Y'),
                'time' => $trip->arrive_at->format('g:i A')
            ],
            'base_fare' => $trip->base_fare,
            'boardingPoint' => [
                [
                    'id' => $boardingPoint->terminal->id,
                    'name' => $boardingPoint->terminal->name,
                    'route_id' => $boardingPoint->id
                ]
            ],
            'droppingPoints' => $droppingPoints
        ];

        return response()->json([
            'status' => true,
            'results' => $results
        ]);

    }


    // main functions

    public function searchApi(Request $request, $type = 'normal'){

        $request->validate([
            'origin' => 'required',
        ]);

        if($request->origin == $request->destination){
            return response()->json([
                'status' => false,
                'message' => ''
            ]);
        }

        $cityOrigin = City::with('terminals')->where('name', $request->origin)->first();
        $cityDestination = City::with('terminals')->where('name', $request->destination)->first();

        $trips = Trip::whereHas('routes', function($query)use($cityOrigin){
            $query->where(function($q) use($cityOrigin){
                $q->where('city_id', $cityOrigin->id)->where('type', 0);
            });
        });
        $trips = $trips->whereHas('routes', function($query)use($cityDestination){
            if(!is_null($cityDestination)){
                $query->where(function($q) use($cityDestination){
                    $q->where('city_id', $cityDestination->id)->where('type', 1);
                });
            }
        });

        if($type == 'tenant'){
            $user = $request->user();
            if($user->hasRole('tenant')){
                $trips = $trips->whereHas('busNoScope', function($q) use($user){
                    $q->where('liner_id', $user->liner_id);
                });
            }
        }

        $trips = $trips->with([
            'routes.terminal.city',
            'bus.liner',
            'bus.seatlayout',
            'tickets' => function($q){
                $q->whereIn('status', [Ticket::PAID, Ticket::PENDING]);
            }
        ]);

        $date = Carbon::createFromFormat('d/m/Y', $request->date);

        $trips = $trips->whereDate('depart_at', $date)->latest()->get();

        $trips = $trips->reject(function($item){
            return $item->depart_at->isPast();
        });

        return response()->json([
            'status' => true,
            'results' => TripResource::collection($trips)
        ]);

    }

    public function search(Request $request){

        $search = [
            'origin' => $request->origin,
            'destination' => $request->destination,
            'seat_count' => $request->seat_count,
            'date' => $request->date
        ];
        return view('trips.search', compact('search'));
    }

}
