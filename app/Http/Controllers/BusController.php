<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SeatLayout;
use App\Bus;
use App\Liner;

class BusController extends Controller{

    public function index(){
        $buses = Bus::with(['liner', 'seatlayout'])->paginate(10);

        return view('admin.bus.index', compact('buses'));
    }

    public function create(){
        $layouts = SeatLayout::with('liner')->get();

        if(auth()->check()){
            if(auth()->user()->hasRole('admin')){
                $liners = Liner::all();
            }
        }

        return view('admin.bus.create', compact('layouts', 'liners'));
    }

    public function store(Request $request){
        if(!$request->user()){
            abort(403);
        }

        $user = $request->user();

        $request->validate([
            'bus_name' => 'required',
            'bus_number' => 'required',
            'seat_layout' => 'required',
            'bus_type' => 'required',
            'bus_amenities' => 'required|array|min:1',
            'bus_liner' => $user->hasRole('admin') ? 'required' : ''
        ]);

        $seatLayout = SeatLayout::where('id', $request->seat_layout)->first();

        $bus = Bus::create([
            'liner_id' => $user->hasRole('admin') ? $request->bus_liner : $user->liner_id,
            'seat_layout_id' => $request->seat_layout,
            'name' => $request->bus_name,
            'body_number' => $request->bus_number,
            'seat_capacity' => $seatLayout->total_seats,
            'amenities' => $request->bus_amenities,
            'type' => $request->bus_type
        ]);


        flash('Successfully Added')->success();

        return redirect()->route('admin.buses.create');
    }

    public function edit($id){
        $bus = Bus::where('id', $id)->firstOrFail();

        $layouts = SeatLayout::with('liner')->get();

        if(auth()->check()){
            if(auth()->user()->hasRole('admin')){
                $liners = Liner::all();
            }
        }

        return view('admin.bus.edit', compact('bus', 'layouts', 'liners'));

    }

    public function update(Request $request){
        if(!$request->user()){
            abort(403);
        }

        $user = $request->user();

        $bus = Bus::where('id', $request->id)->firstOrFail();

        if(!$user->hasRole('admin')){
            if($user->liner_id != $bus->liner_id){
                abort(403);
            }
        }

        $request->validate([
            'bus_name' => 'required',
            'bus_number' => 'required',
            'seat_layout' => 'required',
            'bus_type' => 'required',
            'bus_amenities' => 'required|array|min:1',
            'bus_liner' => $user->hasRole('admin') ? 'required' : ''
        ]);

        $seatLayout = SeatLayout::where('id', $request->seat_layout)->first();

        $bus->liner_id = $request->bus_liner;
        $bus->seat_layout_id = $request->seat_layout;
        $bus->name = $request->bus_name;
        $bus->body_number = $request->bus_number;
        $bus->seat_capacity = $seatLayout->total_seats;
        $bus->amenities = $request->bus_amenities;
        $bus->type = $request->bus_type;

        $bus->save();

        flash('Successfully Updated')->success();

        return redirect()->route('admin.buses.edit', $bus->id);
    }

    public function destroy($id){
        $bus = Bus::where('id', $id)->firstOrFail();

        $bus->delete();

        flash('Successfully Deleted')->success();

        return redirect()->route('admin.buses.index');
    }

    //API

    public function get(Request $request){
        $buses = Bus::with('liner')->get();

        return response()->json([
            'status' => true,
            'results' => $buses
        ]);
    }
}
