<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Spatie\Permission\Models\Role;
use App\Liner;
use App\User;

class UserController extends Controller{

    public function index(){
        $users = User::with('liner','roles')->paginate();

        return view('admin.users.index', compact('users'));
    }

    public function create(){
        $roles = Role::all();
        $liners = Liner::all();

        return view('admin.users.create', compact('roles', 'liners'));
    }

    public function store(Request $request){

        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required|confirmed',
            'roles' => 'required',
            'liner' => 'required_if:roles,==,tenant'
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'liner_id' => $request->liner
        ]);

        $user->syncRoles([$request->roles]);

        flash('Successfully added')->success();

        return redirect()->route('admin.users.index');
    }

    public function edit($id){
        $user = User::where('id', $id)->firstOrFail();

        $roles = Role::all();
        $liners = Liner::all();

        return view('admin.users.edit', compact('user', 'roles', 'liners'));
    }

    public function update($id, Request $request){
        $user = User::where('id', $id)->firstOrFail();

        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users,email,'.$user->id,
            'roles' => 'required',
            'liner' => 'required_if:roles,==,tenant',
        ]);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->liner_id = $request->liner;

        if($request->password != null){
            $user->password = bcrypt($request->password);
        }

        $user->save();

        $user->syncRoles([$request->roles]);

        flash('Successfully update')->success();

        return redirect()->route('admin.users.index');
    }

    public function destroy($id){
        $user = User::where('id', $id)->firstOrFail();
        $user->delete();

        flash('Successfully deleted')->success();
        return redirect()->route('admin.users.index');
    }

}
