<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\Ticket as TicketResources;

use App\Customer;
use App\Ticket;
use App\Trip;

use App\Invoice;

use BusHelpers;

use Srmklive\PayPal\Services\ExpressCheckout;
use Srmklive\PayPal\Services\AdaptivePayments;
use Illuminate\Support\Facades\Log;
use App\Notifications\InvoicePaid;
use App\Notifications\TicketPaid;
use App\Passenger;


class TicketController extends Controller{

    public function index(Request $request){
        $tickets = Ticket::latest();

        $user = auth()->user();
        if(!$user->hasRole('admin')){
            $tickets = $tickets->whereHas('trip', function($q) use($user){
                $q->whereHas('busNoScope', function($q) use($user){
                    $q->where('liner_id', $user->liner_id);
                });
            });
        }

        if(isset($request->q)){
            $tickets->where('id', (int)$request->q);
        }

        $tickets = $tickets->paginate(10);

        return view('admin.ticket.index', compact('tickets'));
    }

    public function view($id){
        $ticket = Ticket::query();
        $user = auth()->user();
        if(!$user->hasRole('admin')){
            $ticket = $ticket->whereHas('trip', function($q) use($user){
                $q->whereHas('bus', function($q) use($user){
                    $q->where('liner_id', $user->liner_id);
                });
            });
        }

        $ticket = $ticket->where('id', $id)->firstOrFail();

        return view('admin.ticket.view', compact('ticket'));
    }

    public function create(){
        return view('admin.ticket.create');
    }

    public function store(Request $request){
        $trip = Trip::where('id', $request->trip['id'])->with('routes', 'bus')->with(['tickets' => function($q){
            $q->whereIn('status', [Ticket::PENDING, Ticket::PAID]);
        }])->firstOrFail();

        $seatsMessage = [];
        foreach ($trip->tickets as $ticket) {
            foreach($request->reserved_seats as $seat){
                if(in_array($seat, $ticket->reserved_seats)){
                    $seatsMessage[] = 'Seat #'.$seat.' is already reserved';
                }
            }
        }
        if(count($seatsMessage)){
            return response()->json([
                'status' => false,
                'message' => $seatsMessage
            ]);
        }

        $drop = $trip->routes->where('id', $request['droppingPoint'])->first();

        if(!$drop){
            session()->forget('reserve');
            return response()->json([
                'status' => false,
                'message' => 'Invalid Dropping point'
            ]);
        }

        $customer = Customer::firstOrCreate([
            'first_name' => $request['name']['first'],
            'last_name' => $request['name']['last'],
            'email' => 'walkin@customer.com',
            'mobile_number' => $request['mobile']
        ]);

        $totalPrice = 0;
        foreach ($request->reserved_seats as $key => $seat) {
            $totalPrice+= ($trip->base_fare - $drop->fare_deduct);
        }

        $ticket = Ticket::create([
            'trip_id' => $trip->id,
            'customer_id' => $customer->id,
            'route_id' => $drop->id,
            'reserved_seats' => $request->reserved_seats,
            'amount' => $totalPrice,
            'status' => Ticket::PAID,
        ]);

        $invoice = Invoice::create([
            'ticket_id' => $ticket->id,
            'customer_id' => $customer->id,
            'amount' => $ticket->amount,
            'type' => 3,
            'transaction_id' => base64_encode('CASH#'.$ticket->id),
        ]);

        return response()->json([
            'status' => true,
            'ticket_no' => $ticket->ticket_no,
            'trans_id' => $invoice->transaction_id,
            'message' => 'Successfully Reserved'
        ]);
    }

    public function reservations(Request $request){

        $tickets = Ticket::where('trip_id', $request->id)->whereIn('status', [Ticket::PAID, Ticket::PENDING])->get();

        return response()->json([
            'status' => true,
            'results' => TicketResources::collection($tickets)
        ]);

    }

    public function reserveSession(Request $request){
        session()->put('reserve', $request->all());
        return response()->json([
            'status' => true
        ]);
    }
    //

    public function book(){
        $reservationInfo = session()->get('reserve');

        if($reservationInfo){
            return view('trips.book', compact('reservationInfo'));
        }else{
            return redirect()->route('trip.search');
        }
    }


    public function checkout(Request $request){

        $trip = Trip::where('id', $request->trip['id'])->with('routes', 'bus')->with(['tickets' => function($q){
            $q->whereIn('status', [Ticket::PENDING, Ticket::PAID]);
        }])->firstOrFail();

        $seatsMessage = [];
        foreach ($trip->tickets as $ticket) {
            foreach($request->reserved_seats as $seat){
                if(in_array($seat, $ticket->reserved_seats)){
                    $seatsMessage[] = 'Seat #'.$seat.' is already reserved';
                }
            }
        }

        if(count($seatsMessage)){
            session()->forget('reserve');
            if($request->payment_method == 'paypal'){
                $provider = new ExpressCheckout;
                $response = $provider->refundTransaction($request->paypal['transaction_id']);

                if($response['ACK'] == 'Success'){
                    $seatsMessage[] = 'Successfully Refunded';
                }else{
                    $seatsMessage[] = 'Someting went wrong in Refunding';
                }

                return response()->json([
                    'status' => false,
                    'message' => $seatsMessage
                ]);
            }else{
                return response()->json([
                    'status' => false,
                    'message' => $seatsMessage
                ]);
            }
        }

        $drop = $trip->routes->where('id', $request['droppingPoint'])->first();

        if(!$drop){
            session()->forget('reserve');
            return response()->json([
                'status' => false,
                'message' => 'Invalid Dropping point'
            ]);
        }

        $customer = Customer::firstOrCreate([
            'first_name' => $request['name']['first'],
            'last_name' => $request['name']['last'],
            'email' => $request['email'],
            'mobile_number' => $request['mobile']
        ]);

        $totalPrice = 0;
        foreach ($request->reserved_seats as $key => $seat) {
            $totalPrice+= ($trip->base_fare - $drop->fare_deduct);
        }

        $totalPrice = BusHelpers::serviceFee($totalPrice);

        $ticket = Ticket::create([
            'trip_id' => $trip->id,
            'customer_id' => $customer->id,
            'route_id' => $drop->id,
            'reserved_seats' => $request->reserved_seats,
            'amount' => $totalPrice,
            'status' => Ticket::PENDING,
        ]);

        foreach($request->reserved_seats as $key => $seat){
            Passenger::create([
                'name' => $request->passengers[$key],
                'ticket_id' => $ticket->id,
                'seat_no' => $seat
            ]);
        }

        if($request->payment_method == 'stripe'){
            try{
                $charge = $customer->charge(\BusHelpers::toCents($totalPrice), [
                    'source' => $request['stripe']['id']
                ]);
            }catch(Exception $e){
                $ticket->status = Ticket::CANCELLED;
                $ticket->save();
            }

            if($charge->paid){

                $invoice = Invoice::create([
                    'ticket_id' => $ticket->id,
                    'customer_id' => $customer->id,
                    'amount' => $ticket->amount,
                    'type' => 1,
                    'stripe_id' => $request['stripe']['id'],
                    'card_brand' => $request['stripe']['card']['brand'],
                    'card_last_four' => $request['stripe']['card']['last4'],
                    'transaction_id' => $charge->id
                ]);

                $ticket->status = Ticket::PAID;

                $invoice->load('customer');

                $customer->notify(new InvoicePaid($invoice));

                if($request['mobile']){
                    $customer->notify(new TicketPaid($invoice, $ticket));
                }

            }else{
                $ticket->status = Ticket::CANCELLED;
            }

            $ticket->save();
            session()->forget('reserve');

            return response()->json([
                'status' => true,
                'transaction_id' => $invoice->transaction_id,
                'message' => $charge->outcome['seller_message']
            ]);

        }elseif($request->payment_method == 'paypal'){

            $provider = new ExpressCheckout;

            $response = $provider->getTransactionDetails($request->paypal['transaction_id']);

            if(isset($response['L_SEVERITYCODE0'])){
                $ticket->status = Ticket::CANCELLED;
                $message = 'Payment cancelled';
            }

            if($response['PAYERID'] == $request->paypal['payer_id']){

                $invoice = Invoice::create([
                    'ticket_id' => $ticket->id,
                    'customer_id' => $customer->id,
                    'amount' => $ticket->amount,
                    'type' => 2,
                    'paypal_email' => $response['EMAIL'],
                    'transaction_id' => $response['TRANSACTIONID']
                ]);

                $ticket->status = Ticket::PAID;

                $invoice->load('customer');

                $customer->notify(new InvoicePaid($invoice));

                if($request['mobile']){
                    $customer->notify(new TicketPaid($invoice, $ticket));
                }

                $message = 'Payment succeeded';
            }

            $ticket->save();

            session()->forget('reserve');
            return response()->json([
                'status' => true,
                'transaction_id' => $invoice->transaction_id,
                'message' => $message,
            ]);

        }else{
            return response()->json([
                'status' => false,
                'message' => 'Invalid Payment method'
            ]);
        }
        // dd($request->all());
    }

    public function success($id){
        $invoice = Invoice::where('transaction_id', $id)->firstOrFail();

        return view('success', compact('invoice'));
    }

    public function information($ticketID, $checkoutID){
        $invoice = Invoice::where('ticket_id', $ticketID)->where('transaction_id', $checkoutID)->firstOrFail();


        $ticket = Ticket::where('id', $invoice->ticket_id)
                        ->with([
                            'customer', 'trip.routes.terminal.city', 'route.terminal.city', 'trip.bus.liner'
                        ])
                        ->firstOrFail();
        $trip = $ticket->trip;
        $customer = $ticket->customer;
        $route = $ticket->route;
        $passengers = $ticket->passengers;

        return view('ticket', compact('ticket', 'trip', 'customer', 'route', 'invoice', 'passengers'));
    }


}
