<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Location\Terminal;
use App\Location\City;
use App\Location\Province;

class LocationController extends Controller{

    public function index(){
        $provinces = Province::withCount(['terminals', 'cities'])->paginate(10);

        return view('admin.location.index', compact('provinces'));
    }

    public function view($id){
        $province = Province::where('id', $id)->with(['cities' => function($q){
            $q->withCount('terminals');
        }])->firstOrFail();

        return view('admin.location.province.view', compact('province'));
    }

    public function terminals(Request $request){
        $dPoints = array();
        $dLocations = array_pluck((array)$request->droppingPoints, 'location');
            foreach($dLocations as $locations){
                if(is_array($locations)){
                    $location = array_first($locations);
                    if(is_array($location)){
                        $dPoints[] = $location['id'];
                    }
                }
            }
            // dd($request->boardingPoint);
        // $dPoints = array_pluck((array)$dLocations);
        $bPoints = is_array($request->boardingPoint) ? array_first($request->boardingPoint)['id'] : null;

        array_push($dPoints, $bPoints);

        $clean = array_filter($dPoints);

        $terminals = Terminal::where('name', 'LIKE', '%'.$request->q.'%')->whereNotIn('id', $clean)->select('id', 'name')->get();

        return response()->json([
            'status' => true,
            'results' => $terminals,
        ]);
    }

    public function provinces(){
        $provinces = Province::with('cities')->get();

        return response()->json([
            'status' => true,
            'results' => $provinces
        ]);
    }

}
