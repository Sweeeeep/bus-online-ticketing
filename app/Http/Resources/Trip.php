<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use BusHelpers;

class Trip extends JsonResource{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'origin' => $this->when(isset($this->routes), function(){
                $origin = $this->routes->where('type', 0)->first();
                if($origin){
                    if($origin->terminal && $origin->terminal->city){
                        return $origin->terminal->city->name;
                    }
                }
            }),
            'destination' => $this->when(isset($this->routes), function(){
                $origin = $this->routes->where('type', 1)->first();
                if($origin){
                    if($origin->terminal && $origin->terminal->city){
                        return $origin->terminal->city->name;
                    }
                }
            }),
            'liner' => $this->when(isset($this->bus), function(){
                $liner = $this->bus->liner;
                if($liner){
                    return $liner;
                }
            }),
            'bus' => $this->when(isset($this->bus), function(){
                $bus = $this->bus;
                return [
                    'name' => $bus->name,
                    'number' => $bus->body_number,
                    'type' => BusHelpers::BusType[$bus->type],
                    'seat_map' => $bus->seatlayout->seat_map,
                    'seat_capacity' => $bus->seat_capacity,
                    'amenities' => $bus->amenities
                ];
            }),
            'departure_time' => $this->depart_at->format('g:i A'),
            'tickets' => Ticket::collection($this->whenLoaded('tickets')),
            'isP2P' => $this->whenLoaded('routes', function(){
                return count($this->routes) == 2 ? true : false;
            }),
            'routes' => $this->when($this->routes, function(){
                return [
                    'boarding' => Route::collection($this->routes->where('type', 0)),
                    'dropping' => Route::collection($this->routes->where('type', 1)),
                ];
            }),
            'fare' => $this->base_fare,
            'available_seats' => $this->available_seats
        ];
    }

}
