<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles;
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'liner_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function liner(){
        return $this->belongsTo(Liner::class);
    }
}
