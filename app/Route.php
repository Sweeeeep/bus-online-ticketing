<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Route extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'terminal_id', 'fare_deduct', 'type', 'sequence', 'trip_id', 'city_id'
    ];

    protected $dates = [
        'deleted_at'
    ];

    public function terminal(){
        return $this->belongsTo(Location\Terminal::class);
    }

    public function trip(){
        return $this->belongsTo(Trip::class);
    }
}
